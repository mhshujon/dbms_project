<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/30/2018
 * Time: 2:59 AM
 */
if (!empty($_POST['touristSpots']) AND !empty($_POST['areaName'])){
    session_start();
    include 'AreaClass.php';
    $object = new AreaClass();
    $object->set($_POST);
    $object->store();
    $_SESSION['areaStatus'] = 'success';
    header('location: area.php');
}
else
    header('location: area.php');