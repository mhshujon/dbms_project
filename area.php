<?php
session_start();
if (isset($_SESSION['index'][0]['type'])){
    if ($_SESSION['index'][0]['type'] == 'admin'){
        if (isset($_SESSION['areaStatus'])){
            if ($_SESSION['areaStatus'] == 'success'){
                echo "<script>window.alert('Data successfully updated')</script>";
                $_SESSION['areaStatus'] = '';
            }
        }
    }
    else
        header('location: index.php');
}
else
    header('location: index.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>The Travellers</title>
    <base href="http://localhost/dbms_project/"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link href="assets/signup/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="assets/login/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100" style="background-image: url('assets/blog/story_form/images/bg-01.jpg');">
        <div class="wrap-login100">
            <form class="signup-form validate-form" method="POST" action="storeArea.php">
					<span class="login100-form-title">
						Area Input
					</span>

                <div class="row row-space">
                    <div class="col-6">
                        <div class="wrap-input100 validate-input" data-validate = "Please enter a valid area name.">
                            <input class="input100" type="text" name="areaName" placeholder="Area Name">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="wrap-input100 validate-input" data-validate = "Please enter a valid spot name.">
                            <input class="input100" type="text" name="touristSpots" placeholder="Tourist Spots">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div><div class="container-login100-form-btn">
                    <button class="login100-form-btn" type="submit">
                        Submit
                    </button>
                </div>
                <div class="container-login100-form-btn">
                    <a href="data_input.php">
                        <button class="login100-form-btn btn-danger" type="button">
                            Cancel
                        </button>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="assets/signup/vendor/jquery/jquery.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/bootstrap/assets/login/js/popper.js"></script>
<script src="assets/login/vendor/bootstrap/assets/login/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/tilt/tilt.jquery.min.js"></script>
<script src="assets/signup/vendor/datepicker/moment.min.js"></script>
<script src="assets/signup/vendor/datepicker/daterangepicker.js"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="assets/login/js/main.js"></script>
<!--===============================================================================================-->
<script src="assets/signup/js/global.js"></script>

</body>
</html>
