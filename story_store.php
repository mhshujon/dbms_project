<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/29/2018
 * Time: 3:04 AM
 */
if (empty($_POST) AND empty($_SESSION)){
    header('location: index.php');
}
else{
    include('BlogClass.php');
    session_start();

//    $email = $_POST['email'];
    $_POST['date'] = date("Y-m-d");
    $_POST['usersEmail'] = $_SESSION['email'];
    $object = new BlogClass();
//    var_dump($_POST['date']);
//var_dump($_POST);

//    $eml = $object->email_validation($email);

    try{
        $object->set($_POST);
        $object->store();
        $_SESSION['storyStore']='success';
        header('location: blog.php');
    }
    catch(PDOException $ex){
        echo "<script>window.location.assign('login.php?status=dberror')</script>";
    }
}