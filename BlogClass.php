<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/25/2018
 * Time: 11:49 PM
 */
include ('Connection.php');

class BlogClass extends Connection
{
    private $storyID = "NULL";
    private $usersEmail;
    private $tittle;
    private $destinationFrom_areaID;
    private $destinationTO_areaID;
    private $duration;
    private $transportBY;
    private $transportCost;
    private $totalFoodCost;
    private $totalCompanion;
    private $totalCost;
    private $description = "NULL";
    private $image = "NULL";
    private $date;

    public function set($data = array()){
        if (array_key_exists('storyID', $data)) {
            $this->storyID = $data['storyID'];
        }
        if (array_key_exists('usersEmail', $data)) {
            $this->usersEmail = $data['usersEmail'];
        }
        if (array_key_exists('tittle', $data)) {
            $this->tittle = $data['tittle'];
        }
        if (array_key_exists('destinationFrom_areaID', $data)) {
            $this->destinationFrom_areaID = $data['destinationFrom_areaID'];
        }
        if (array_key_exists('destinationTO_areaID', $data)) {
            $this->destinationTO_areaID = $data['destinationTO_areaID'];
        }
        if (array_key_exists('duration', $data)) {
            $this->duration = $data['duration'];
        }
        if (array_key_exists('transportBY', $data)) {
            $this->transportBY = $data['transportBY'];
        }
        if (array_key_exists('transportCost', $data)) {
            $this->transportCost = $data['transportCost'];
        }
        if (array_key_exists('totalFoodCost', $data)) {
            $this->totalFoodCost = $data['totalFoodCost'];
        }
        if (array_key_exists('totalCompanion', $data)) {
            $this->totalCompanion = $data['totalCompanion'];
        }
        if (array_key_exists('totalCost', $data)) {
            $this->totalCost = $data['totalCost'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }
        if (array_key_exists('date', $data)) {
            $this->date = $data['date'];
        }
////        var_dump($this);
        return $this->usersEmail;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `stories` (`storyID`, `usersEmail`, `tittle`, `destinationFrom_areaID`, `destinationTO_areaID`, `duration`, `transportBY`, `transportCost`, `totalFoodCost`, `totalCompanion`, `totalCost`, `description`, `image`, `date`) VALUES (:storyID, :usersEmail, :tittle, :destinationFrom_areaID, :destinationTO_areaID, :duration, :transportBY, :transportCost, :totalFoodCost, :totalCompanion, :totalCost, :description, :image, :date)");
            $result =  $stmt->execute(array(
                ':storyID' => $this->storyID,
                ':usersEmail' => $this->usersEmail,
                ':tittle' => $this->tittle,
                ':destinationFrom_areaID' => $this->destinationFrom_areaID,
                ':destinationTO_areaID' => $this->destinationTO_areaID,
                ':duration' => $this->duration,
                ':transportBY' => $this->transportBY,
                ':transportCost' => $this->transportCost,
                ':totalFoodCost' => $this->totalFoodCost,
                ':totalCompanion' => $this->totalCompanion,
                ':totalCost' => $this->totalCost,
                ':description' => $this->description,
                ':image' => $this->image,
                ':date' => $this->date
            ));

//            var_dump('this->usersEmail');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                $_SESSION['msg'] = 'Data successfully Inserted !!';
//                header('location:index.php');
//                echo $this->usersEmail;
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `stories` ORDER BY storyID DESC");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function count(){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `stories`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function user_validation($storyID, $pass){
        try{
            $stmt = $this->con->prepare("SELECT storyID, pass FROM `users` WHERE `storyID`='$storyID' AND `pass`='$pass'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function storyID_validation($storyID){
        try{
            $stmt = $this->con->prepare("SELECT storyID FROM `users` WHERE `storyID`='$storyID'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

//    public function view($id){
//        try{
//            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            return $stmt->fetch(PDO::FETCH_ASSOC);
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }
//
//    public function delete($id){
//        try{
//            $stmt = $this->con->prepare("DELETE FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('location:index.php');
//            }
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }

    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`users` SET `firstName` = :firstName, `lastName` = :lastName, `storyID` = :storyID, `usersEmail` = :usersEmail, `dob` = :dob, `gender` = :gender, `address` = :address WHERE `storyID` = :storyID;");

            $result =  $stmt->execute(array(
                ':firstName' => $this->firstName,
                ':lastName' => $this->lastName,
                ':storyID' => $this->storyID,
                ':usersEmail' => $this->usersEmail,
                ':dob' => $this->dob,
                ':gender' => $this->gender,
                ':address' => $this->address
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                echo 'Yes'.$this->storyID;
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}