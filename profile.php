<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/27/2018
 * Time: 10:48 PM
 */
session_start();
if (empty($_SESSION)){
    header('location: index.php');
}
if (!empty($_SESSION)){
    $name_1='';
    $name_2='';
    $email='';
    $address='';
    $mobileNumber='';
    $gender='';
    $dob='';
//    var_dump($_SESSION['index']);
    if (isset($_SESSION['index'])){
        foreach ($_SESSION['index'] as $key=>$val){
            $value=$val;
        }
    }
    $name_1=$value['firstName'];
    $name_2=$value['lastName'];
    $email=$value['email'];
    $address=$value['address'];
    $mobileNumber=$value['mobileNumber'];
    $gender=$value['gender'];
    $dob=$value['dob'];

    if (!empty($_SESSION['updateStatus'])){
        if ($_SESSION['updateStatus'] == 'emlExists'){
            $_SESSION['updateStatus']='';
            echo "<script>window.alert('This email is already registered!')</script>";
        }
        if ($_SESSION['updateStatus'] == 'success'){
            $_SESSION['updateStatus']='';
            echo "<script>window.alert('Info updated successfully!')</script>";
        }
    }
}
//var_dump($_SESSION);
//if (!empty($_SESSION)){
//    if (empty($_SESSION['regStatus'])){
//        if ($_SESSION['login_status'] != 'invalid')
//            header('location: index.php');
//    }
//    else{
//        if ($_SESSION['regStatus'] == 'emlExists'){
//            echo "<script>window.alert('This email is already registered!')</script>";
//            session_destroy();
//        }
//    }
//}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>The Travellers</title>
    <base href="http://localhost/dbms_project/"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="assets/login/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="assets/login/images/img-01.png" alt="IMG">
            </div>
            <table class="login100-form table table-user-information">
                <tbody>
                    <tr>
                        <td>Full Name</td>
                        <td><?php echo $name_1.' '.$name_2?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><a href="mailto:<?php echo $email?>"><?php echo $email?></a></td>
                    </tr>
                    <tr>
                        <td>Home Address</td>
                        <td><?php echo $address?></td>
                    </tr>
                    <tr>
                        <td>Phone Number</td>
                        <td><?php echo $mobileNumber?></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td><?php echo $gender?></td>
                    </tr>
                    <tr>
                        <td>Date of Birth</td>
                        <td><?php echo $dob?></td>
                    </tr>
                </tbody>
            </table>
<!--            <div class="container-login100-form-btn">-->
<!--                <a href="update.php">-->
<!--                    <button class="btn btn--radius btn-info" type="button">Update</button>-->
<!--                </a>-->
<!--            </div>-->
            <div class="container-login100-form-btn">
                <div class="row row-space">
                    <div class="wrap-input">
                        <div class="col-2">
                            <a href="index.php">
                                <button class="btn btn--radius btn-info" type="button">Home</button>
                            </a>
                        </div>
                    </div>

                    <div class="wrap-input">
                        <div class="col-2">
                            <a href="update.php">
                                <button class="btn btn--radius btn-info" type="button">Update</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/bootstrap/assets/login/js/popper.js"></script>
<script src="assets/login/vendor/bootstrap/assets/login/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/tilt/tilt.jquery.min.js"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="assets/login/js/main.js"></script>

</body>
</html>
