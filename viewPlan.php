<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/30/2018
 * Time: 6:29 PM
 */
if (!empty($_POST['tourFrom']) AND !empty($_POST['tourTO'])){
    session_start();
    include ('TransportClass.php');
//
    $objTransport = new TransportClass();
//    $objHotel = new HotelClass();

    $areaFrom = $_POST['tourFrom'];
    $areaTO = $_POST['tourTO'];

    $_SESSION['viewTransport']= $objTransport->viewPlanTransport($areaFrom, $areaTO);
    $_SESSION['viewHotel']= $objTransport->viewPlanHotel($areaTO);
    $_SESSION['viewArea']= $objTransport->viewPlanArea($areaTO);
    if (isset($_SESSION['viewTransport'])){
        $count1 = $objTransport->viewPlanTransportCount($areaFrom, $areaTO);
        $col1 = $count1[0]['col'];
        $_SESSION['colT']=$col1;
    }
    if (isset($_SESSION['viewHotel'])){
        $count2 = $objTransport->viewPlanHotelCount($areaTO);
        $col2 = $count2[0]['col'];
        $_SESSION['colH']=$col2;
    }
    if (isset($_SESSION['viewArea'])){
        $count3 = $objTransport->viewPlanAreaCount($areaTO);
        $col3 = $count3[0]['col'];
        $_SESSION['colA']=$col3;
    }
    header('location: viewTourPlan.php');
//    var_dump($_SESSION['viewTransport']);
//    var_dump($_SESSION['viewHotel']);
//    echo 'Area TO: '.$areaTO;
//    var_dump($_SESSION['viewArea']);

}
else{
    header('location: viewTourPlan.php');
}