<?php
session_start();
//var_dump($_SESSION);
if (empty($_SESSION)){
    header('location: index.php');
}
elseif (!empty($_SESSION)){
    $name_1='';
    $name_2='';
    $email='';
    $address='';
    $mobileNumber='';
    $gender='';
    $dob='';
    foreach ($_SESSION['index'] as $key=>$val){
        $value=$val;
    }
    if ($value){
        $name_1=$value['firstName'];
        $name_2=$value['lastName'];
        $email=$value['email'];
        $address=$value['address'];
        $mobileNumber=$value['mobileNumber'];
        $gender=$value['gender'];
        $dob=$value['dob'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>The Travellers</title>
    <base href="http://localhost/dbms_project/"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link href="assets/signup/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="assets/login/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <!--            <div class="login100-pic js-tilt" data-tilt>-->
            <!--                <img src="assets/login/images/img-01.png" alt="IMG">-->
            <!--            </div>-->

            <form class="signup-form validate-form" method="POST" action="updateinfo.php">

                <input style="visibility: hidden" class="input100" type="text" name="email" value="<?php echo $email?>">
                <span class="login100-form-title">
                    Update Information
                </span>
                <div class="row row-space">
                    <div class="col-6">
                        <div class="wrap-input100 validate-input" data-validate = "First name is required!">
                            <input class="input100" type="text" name="firstName" value="<?php echo $name_1?>">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="wrap-input100 validate-input" data-validate = "Last name is required!">
                            <input class="input100" type="text" name="lastName" value="<?php echo $name_2?>">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row row-space">
                    <div class="col-6">
                        <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                            <input disabled="disabled" class="input100" type="text" name="email" value="<?php echo $email?>">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="wrap-input100">
                            <input class="input100" type="text" name="mobileNumber" value="<?php echo $mobileNumber?>">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-phone" aria-hidden="true"></i>
						    </span>
                        </div>
                    </div>
                </div>

<!--                <div class="row row-space">-->
<!--                    <div class="col-6">-->
<!--                        <div class="wrap-input100 validate-input" data-validate = "Password is required">-->
<!--                            <input class="input100" type="password" name="pass" placeholder="Password">-->
<!--                            <span class="focus-input100"></span>-->
<!--                            <span class="symbol-input100">-->
<!--							    <i class="fa fa-lock" aria-hidden="true"></i>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-6">-->
<!--                        <div class="wrap-input100 validate-input" data-validate = "Please re-type your password">-->
<!--                            <input class="input100" type="password" name="pass_1" placeholder="Re-type password">-->
<!--                            <span class="focus-input100"></span>-->
<!--                            <span class="symbol-input100">-->
<!--							    <i class="fa fa-lock" aria-hidden="true"></i>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="row row-space">
                    <!--                    <div class="col-2">-->
                    <!--                        <div class="input-group">-->
                    <!--                            <input class="input--style-1 js-datepicker" type="text" placeholder="BIRTHDATE" name="dob">-->
                    <!--                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <div class="col-6">
                        <div class="wrap-input100">
                            <input class="input100 input--style-1 js-datepicker" type="text" name="dob" value="<?php echo $dob?>">
                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-calendar-check" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="wrap-input100">
                            <select class="input100" name="gender" style="border-style: hidden">
                                <option selected="selected" value="<?php echo $gender?>"><?php echo $gender?></option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>
                            </select>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-transgender-alt" aria-hidden="true"></i>
                            </span>
                            <!--                            <div class="select-dropdown"></div>-->
                        </div>
                    </div>
                </div>

                <div class="row row-space">
                    <div class="wrap-input100" style="margin-left: 15px; padding-right: 15px">
                        <input class="input100 input--style-1" type="text" name="address" value="<?php echo $address?>">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
							    <i class="fa fa-map-marked-alt" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>

                <div class="row row-space">
                    <div class="col-12">
                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn" type="submit">
                                Update
                            </button>
                        </div>
                    </div>
                </div>

                <?php
                if(isset($_SESSION['login_status'])){
                    $login_status = $_SESSION['login_status'];
                    if($login_status == 'invalid'){
                        echo "<script>window.alert('Wrong email or password!')</script>";
                    }
                    else if($login_status=='dberror'){
                        echo "<script>window.alert('Database Connection Error')</script>";
                    }
                }
                //                session_destroy();
                ?>
            </form>

            <div class="signup-form" style="margin-top: -50px; margin-right: 30px; width: inherit">
                <div class="row row-space">
                    <div class="col-12">
                        <a href="profile.php">
                            <div class="container-login100-form-btn">
                                <button class="btn login100-form-btn">
                                    Cancel
                                </button>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="assets/signup/vendor/jquery/jquery.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/bootstrap/assets/login/js/popper.js"></script>
<script src="assets/login/vendor/bootstrap/assets/login/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/tilt/tilt.jquery.min.js"></script>
<script src="assets/signup/vendor/datepicker/moment.min.js"></script>
<script src="assets/signup/vendor/datepicker/daterangepicker.js"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="assets/login/js/main.js"></script>
<!--===============================================================================================-->
<script src="assets/signup/js/global.js"></script>

</body>
</html>
