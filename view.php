<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/27/2018
 * Time: 11:19 PM
 */
session_start();
if (empty($_SESSION)){
    header('location: index.php');
}
elseif(!empty($_SESSION['email'])){
    include('Users.php');

    $email = $_SESSION['email'];

    $object = new Users();

    $_SESSION['index'] = $object->index($email);

    header('location: profile.php');
}
