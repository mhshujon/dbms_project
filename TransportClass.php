<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/25/2018
 * Time: 11:49 PM
 */
include ('Connection.php');

class TransportClass extends Connection
{
    private $transportID;
    private $transportName;
    private $transportCategory;
    private $transport_areaIDFrom;
    private $transport_areaIDTO;
    private $transportID_categoryTypePrice;
    private $transportCategoryType;
    private $transportCategoryTypePrice;

    public function set($data = array()){
        if (array_key_exists('transportID', $data)) {
            $this->transportID = $data['transportID'];
        }
        if (array_key_exists('transportName', $data)) {
            $this->transportName = $data['transportName'];
        }
        if (array_key_exists('transportCategory', $data)) {
            $this->transportCategory = $data['transportCategory'];
        }
        if (array_key_exists('transport_areaIDFrom', $data)) {
            $this->transport_areaIDFrom = $data['transport_areaIDFrom'];
        }
        if (array_key_exists('transport_areaIDTO', $data)) {
            $this->transport_areaIDTO = $data['transport_areaIDTO'];
        }
        if (array_key_exists('transportID_categoryTypePrice', $data)) {
            $this->transportID_categoryTypePrice = $data['transportID_categoryTypePrice'];
        }
        if (array_key_exists('transportCategoryType', $data)) {
            $this->transportCategoryType = $data['transportCategoryType'];
        }
        if (array_key_exists('transportCategoryTypePrice', $data)) {
            $this->transportCategoryTypePrice = $data['transportCategoryTypePrice'];
        }
////        var_dump($this);
//        return $this->usersEmail;
    }

    public function store(){
        try{
            $stmt1 = $this->con->prepare("INSERT INTO `transportation` (`transportID`, `transportName`, `transportCategory`, `transport_areaIDFrom`, `transport_areaIDTO`) VALUES (:transportID, :transportName, :transportCategory, :transport_areaIDFrom, :transport_areaIDTO)");
            $stmt1->execute(array(
                ':transportID' => $this->transportID,
                ':transportName' => $this->transportName,
                ':transportCategory' => $this->transportCategory,
                ':transport_areaIDFrom' => $this->transport_areaIDFrom,
                ':transport_areaIDTO' => $this->transport_areaIDTO
            ));

            $stmt2 = $this->con->prepare("INSERT INTO `transport_categorytypeprice` (`transportID_categoryTypePrice`, `transportCategoryType`, `transportCategoryTypePrice`) VALUES (:transportID_categoryTypePrice, :transportCategoryType, :transportCategoryTypePrice)");
            $stmt2->execute(array(
                ':transportID_categoryTypePrice' => $this->transportID,
                ':transportCategoryType' => $this->transportCategoryType,
                ':transportCategoryTypePrice' => $this->transportCategoryTypePrice
            ));

//            var_dump('this->usersEmail');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt1->errorInfo();
//            print_r($arr);
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt2->errorInfo();
//            print_r($arr);
//            if($result){
//                $_SESSION['msg'] = 'Data successfully Inserted !!';
//                header('location:index.php');
//                echo $this->usersEmail;
//            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `hotel`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function viewPlanArea($areaTO){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `area` WHERE areaID LIKE '$areaTO'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function viewPlanTransport($areaFrom, $areaTO){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `transportation` AS T1 JOIN transport_categorytypeprice AS T2 ON T2.transportID_categoryTypePrice=T1.transportID WHERE transport_areaIDFrom LIKE '$areaFrom' AND transport_areaIDTO LIKE '$areaTO'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function viewPlanHotel($areaTO){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `hotel` AS H1 JOIN hoteltypeprice AS H2 ON H2.hotelID_TypePrice=H1.hotelID WHERE hotel_areaID LIKE '$areaTO'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function viewPlanAreaCount($areaTO){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `area` WHERE areaID LIKE '$areaTO'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function viewPlanTransportCount($areaFrom, $areaTO){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `transportation` AS T1 JOIN transport_categorytypeprice AS T2 ON T2.transportID_categoryTypePrice=T1.transportID WHERE transport_areaIDFrom LIKE '$areaFrom' AND transport_areaIDTO LIKE '$areaTO'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function viewPlanHotelCount($areaTO){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `hotel` AS H1 JOIN hoteltypeprice AS H2 ON H2.hotelID_TypePrice=H1.hotelID WHERE hotel_areaID LIKE '$areaTO'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function count(){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `hotel`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function storyID($storyID){
        try{
            $stmt = $this->con->prepare("SELECT CONCAT(firstName, ' ', lastName)FROM `users` WHERE  `storyID`='$storyID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function user_validation($storyID, $pass){
        try{
            $stmt = $this->con->prepare("SELECT storyID, pass FROM `users` WHERE `storyID`='$storyID' AND `pass`='$pass'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function storyID_validation($storyID){
        try{
            $stmt = $this->con->prepare("SELECT storyID FROM `users` WHERE `storyID`='$storyID'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

//    public function view($id){
//        try{
//            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            return $stmt->fetch(PDO::FETCH_ASSOC);
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }
//
//    public function delete($id){
//        try{
//            $stmt = $this->con->prepare("DELETE FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('location:index.php');
//            }
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }

    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`users` SET `firstName` = :firstName, `lastName` = :lastName, `storyID` = :storyID, `usersEmail` = :usersEmail, `dob` = :dob, `gender` = :gender, `address` = :address WHERE `storyID` = :storyID;");

            $result =  $stmt->execute(array(
                ':firstName' => $this->firstName,
                ':lastName' => $this->lastName,
                ':storyID' => $this->storyID,
                ':usersEmail' => $this->usersEmail,
                ':dob' => $this->dob,
                ':gender' => $this->gender,
                ':address' => $this->address
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                echo 'Yes'.$this->storyID;
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}