<?php
session_start();
//var_dump($_SESSION);
if (!empty($_SESSION)){
    if (empty($_SESSION['login_status']))
        if (!empty($_SESSION['regStatus'])){
            if ($_SESSION['regStatus'] == 'success'){
                echo "<script>window.alert('Registration Complete!')</script>";
                echo "<script>window.alert('Please login')</script>";
                $_SESSION['regStatus']='';
//                session_destroy();
//                session_start();
            }
        }

    if (isset($_SESSION['index'])){
        $name_1='';
        $name_2='';
        $type='';

        foreach ($_SESSION['index'] as $key=>$val){
            $value=$val;
        }
        if (!empty($value)){
            $name_1=$value['firstName'];
            $name_2=$value['lastName'];
            $type=$value['type'];
        }

        if (isset($_SESSION['welcome'])){
            if ($_SESSION['welcome'] == 'yes'){
                $_SESSION['welcome'] = '';
                echo "<script>window.alert('Logged in successfully')</script>";
            }
        }
    }
}
?>

<!DOCTYPE html>
<!--[if lt IE 9 ]><html class="no-js oldie" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>The Travellers</title>
    <base href="http://localhost/dbms_project/"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="assets/css/base.css">
    <link rel="stylesheet" href="assets/css/vendor.css">
    <link rel="stylesheet" href="assets/css/main.css">

    <!-- script
    ================================================== -->
    <script src="assets/js/modernizr.js"></script>
    <script src="assets/js/pace.min.js"></script>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="assets/image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top">

<!-- header
================================================== -->
<header class="s-header">

    <div class="header-logo">
        <a class="site-logo" href="index.php">
            <img src="assets/images/logo.png" alt="Homepage">
        </a>
    </div>

    <nav class="header-nav">

        <a href="#0" class="header-nav__close" title="close"><span>Close</span></a>

        <div class="header-nav__content">
            <h3>Navigation</h3>

            <?php
                if (isset($name_1) AND isset($name_2) AND isset($type)){
                    if ($type == 'admin')
                        echo '<h3>Hello '.'<strong>'.'<a href="view.php">'.'<h3>'.$name_1.' '.$name_2.' ('.$type.')'.'</h3>'.'</a>'.'</strong>'.'</h3>';
                    else
                        echo '<h3>Hello '.'<strong>'.'<a href="view.php">'.'<h3>'.$name_1.' '.$name_2.'</h3>'.'</a>'.'</strong>'.'</h3>';
                }
//            var_dump($_SESSION);
            ?>

            <ul class="header-nav__list">
                <li class="current"><a class="smoothscroll"  href="#home" title="home">Home</a></li>
                <li><a class="smoothscroll"  href="#about" title="about">About</a></li>
                <li><a class="smoothscroll"  href="#services" title="services">Services</a></li>
                <li><a class="smoothscroll"  href="#works" title="works">Works</a></li>
                <li><a class="smoothscroll"  href="#clients" title="clients">Clients</a></li>
                <li><a class="smoothscroll"  href="#contact" title="contact">Contact</a></li>
                <?php
                    if (isset($_SESSION['login_status'])){
                        $login_status = $_SESSION['login_status'];
                        if ($login_status == 'success'){
                            if ($type == 'admin'){
                                echo '<li><a class=""  href="data_input.php" title="contact">Data Entry</a></li>';
                                echo '<li><a class="" href="logout.php" title="logout">Logout</a></li>';
                            }
                            else
                                echo '<li><a class="" href="logout.php" title="logout">Logout</a></li>';
//                            session_destroy();
                        }
                        else{
                            echo '<li><a class="" href="login.php" title="login">Login</a> or <a class="" href="signup.php" title="signup">Register</a></li>';
                            session_destroy();
                        }
                    }
                    else{
                        echo '<li><a class="" href="login.php" title="login">Login</a> or <a class="" href="signup.php" title="signup">Register</a></li>';
                        session_destroy();
                    }
                ?>
            </ul>
<!--            <textarea name="" id="" cols="30" rows="10" contenteditable="false"></textarea>-->
            <p>Perspiciatis hic praesentium nesciunt. Et neque a dolorum <a href='#0'>voluptatem</a> porro iusto sequi veritatis libero enim. Iusto id suscipit veritatis neque reprehenderit.</p>

            <ul class="header-nav__social">
                <li>
                    <a href="https://www.facebook.com/mhshujon07"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                    <a href="https://twitter.com/mh_shujon"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                    <a href="https://www.instagram.com/mh_shujon"><i class="fa fa-instagram"></i></a>
                </li>
                <li>
                    <a href="https://gitlab.com/mhshujon"><i class="fa fa-gitlab"></i></a>
                </li>
                <li>
                    <a href="https://www.youtube.com/powerplaygaming"><i class="fa fa-youtube-play"></i></a>
                </li>
            </ul>

        </div> <!-- end header-nav__content -->

    </nav>  <!-- end header-nav -->

    <a class="header-menu-toggle" href="#0">
        <span class="header-menu-text">Menu</span>
        <span class="header-menu-icon"></span>
    </a>

</header> <!-- end s-header -->


<!-- home
================================================== -->
<section id="home" class="s-home target-section" data-parallax="scroll" data-image-src="assets/images/hero-bg.jpg" data-natural-width=3000 data-natural-height=2000 data-position-y=center>

    <div class="overlay"></div>
    <div class="shadow-overlay"></div>

    <div class="home-content">

        <div class="row home-content__main">

            <h3>Welcome to The Travellers</h3>

            <h1>
                We are a group of people <br>
                who organize tour events <br>
                and helps to plan your <br>
                tour more easily.
            </h1>

            <div class="home-content__buttons">
                <a href="<?php
                if (empty($_SESSION)){
                    echo 'login.php';
                }
                else{
                    if (!empty($_SESSION['email']))
                        echo 'story_form.php';
                }

                ?>" class="btn btn--stroke">
                    Write Your Story
                </a>
                <a href="blog.php" class="btn btn--stroke">
                    Visit our blog
                </a>
                <a href="viewEvents.php" class="btn btn--stroke">
                    Look for events
                </a>
                <a href="viewTourPlan.php" class="btn btn--stroke">
                    Plan your tour
                </a>
            </div>

        </div>

        <div class="home-content__scroll">
            <a href="#about" class="scroll-link smoothscroll">
                <span>Scroll Down</span>
            </a>
        </div>

        <div class="home-content__line"></div>

    </div> <!-- end home-content -->


    <ul class="home-social">
        <li>
            <a href="https://www.facebook.com/mhshujon07"><i class="fa fa-facebook"></i><span>Facebook</span></a>
        </li>
        <li>
            <a href="https://twitter.com/mh_shujon"><i class="fa fa-twitter"></i><span>Twiiter</span></a>
        </li>
        <li>
            <a href="https://www.instagram.com/mh_shujon/"><i class="fa fa-instagram"></i><span>Instagram</span></a>
        </li>
        <li>
            <a href="https://gitlab.com/mhshujon"><i class="fa fa-gitlab"></i></i><span>Gitlab</span></a>
        </li>
        <li>
            <a href="https://www.youtube.com/powerplaygaming"><i class="fa fa-youtube-play"></i><span>YouTube</span></a>
        </li>
    </ul>
    <!-- end home-social -->

</section> <!-- end s-home -->