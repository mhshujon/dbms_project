<?php
session_start();

include 'AreaClass.php';
$object = new AreaClass();

$_SESSION['areaIndex'] = $object->index();

if (isset($_SESSION['areaIndex'])){
    $count = $object->count();
    $col = $count[0]['col'];
}
if (isset($_SESSION['viewTransport']) AND isset($_SESSION['colT'])){
    $tCount = $_SESSION['colT'];
}
if (isset($_SESSION['viewHotel']) AND isset($_SESSION['colH'])){
    $hCount = $_SESSION['colH'];
}
if (isset($_SESSION['viewArea']) AND isset($_SESSION['colA'])){
    $aCount = $_SESSION['colA'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>The Travellers</title>
    <base href="http://localhost/dbms_project/"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link href="assets/signup/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="assets/login/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100" style="background-image: url('assets/blog/story_form/images/bg-01.jpg');">
        <div class="wrap-login100">
            <!--            <div class="login100-pic js-tilt" data-tilt>-->
            <!--                <img src="assets/login/images/img-01.png" alt="IMG">-->
            <!--            </div>-->

            <form class="signup-form validate-form" method="POST" action="viewPlan.php">
                <div class="row">
                    <div class="col-7">
                    <span class="login100-form-title">
                            Look For Tour Plan
                    </span>

                        <div class="row row-space">
                            <div class="col-6" data-validate = "This field is required!">
                                <div class="wrap-input100 validate-input">
                                    <select class="input100" name="tourFrom"  value="" style="border-style: hidden">
                                        <option selected disabled>From</option>
                                        <?php if(isset($col))for ($i=0; $i<$col; $i++):?>
                                            <option value="<?php echo $_SESSION['areaIndex'][$i]['areaID'];?>"><?php echo $_SESSION['areaIndex'][$i]['areaName'];?></option>
                                        <?php endfor;?>
                                    </select>
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
							    <i class="fa fa-transgender-alt" aria-hidden="true"></i>
                            </span>
                                    <!--                            <div class="select-dropdown"></div>-->
                                </div>
                            </div>
                            <div class="col-6" data-validate = "This field is required!">
                                <div class="wrap-input100 validate-input">
                                    <select class="input100" name="tourTO"  value="" style="border-style: hidden">
                                        <option selected disabled>To</option>
                                        <?php if(isset($col))for ($i=0; $i<$col; $i++):?>
                                            <option value="<?php echo $_SESSION['areaIndex'][$i]['areaID'];?>"><?php echo $_SESSION['areaIndex'][$i]['areaName'];?></option>
                                        <?php endfor;?>
                                    </select>
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
							    <i class="fa fa-transgender-alt" aria-hidden="true"></i>
                            </span>
                                    <!--                            <div class="select-dropdown"></div>-->
                                </div>
                            </div>
                        </div>

                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn" type="submit">
                                Submit
                            </button>
                        </div>
                        <div class="container-login100-form-btn">
                            <a href="index.php">
                                <button class="login100-form-btn" type="button">
                                    HOME
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="col-5">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Type</th>
                                <th scope="col">Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td scope="col">1</td>
                                <td scope="col">A</td>
                                <td scope="col">Single Bed-2P</td>
                            </tr>
                            <tr>
                                <td scope="col">2</td>
                                <td scope="col">B</td>
                                <td scope="col">Double Bed-4P</td>
                            </tr>
                            <tr>
                                <td scope="col">3</td>
                                <td scope="col">C</td>
                                <td scope="col">Triple Bed-6P</td>
                            </tr>
                            <tr>
                                <td scope="col">4</td>
                                <td scope="col">D</td>
                                <td scope="col">Four Bed-8P</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>

            <div class="row row-space" style="margin-left: 20px">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <!--                        --><?php //if (!empty($tCount) AND !empty($_SESSION['viewTransport'])) for ($i=0; $i<$tCount;$i++):?>
                        <th scope="col">AREA NAME</th>
                        <th scope="col">TOURIST SPOTS</th>
                        <!--                        <th scope="col">--><?php //echo $_SESSION['viewTransport'][$i]['transportName'];?><!--</th>-->
                        <!--                        --><?php //endfor;?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($aCount) AND !empty($_SESSION['viewArea'])) for ($i=0; $i<$aCount;$i++):?>
                        <tr>
                            <td scope="col"><?php $j=1; echo $j=$j+$i?></td>
                            <td scope="col"><?php echo $_SESSION['viewArea'][$i]['areaName'];?></td>
                            <td scope="col"><?php echo $_SESSION['viewArea'][$i]['touristSpots'];?></td>
                        </tr>
                    <?php endfor;?>
                    </tbody>
                </table>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <!--                        --><?php //if (!empty($tCount) AND !empty($_SESSION['viewTransport'])) for ($i=0; $i<$tCount;$i++):?>
                        <th scope="col">COMPANY NAME</th>
                        <th scope="col">TYPE</th>
                        <th scope="col">AC/NON-AC</th>
                        <th scope="col">PRICE</th>
                        <!--                        <th scope="col">--><?php //echo $_SESSION['viewTransport'][$i]['transportName'];?><!--</th>-->
                        <!--                        --><?php //endfor;?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($tCount) AND !empty($_SESSION['viewTransport'])) for ($i=0; $i<$tCount;$i++):?>
                        <tr>
                            <td scope="col"><?php $j=1; echo $j=$j+$i?></td>
                            <td scope="col"><?php echo $_SESSION['viewTransport'][$i]['transportName'];?></td>
                            <td scope="col"><?php echo $_SESSION['viewTransport'][$i]['transportCategory'];?></td>
                            <td scope="col"><?php echo $_SESSION['viewTransport'][$i]['transportCategoryType'];?></td>
                            <td scope="col"><?php echo $_SESSION['viewTransport'][$i]['transportCategoryTypePrice'];?></td>
                        </tr>
                    <?php endfor;?>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <!--                        --><?php //if (!empty($tCount) AND !empty($_SESSION['viewTransport'])) for ($i=0; $i<$tCount;$i++):?>
                        <th scope="col">HOTEL NAME</th>
                        <th scope="col">HOTEL TYPE</th>
                        <th scope="col">ROOM PRICE/NIGHT</th>
                        <th scope="col">CURRENT OFFER(%)</th>
                        <!--                        <th scope="col">--><?php //echo $_SESSION['viewTransport'][$i]['transportName'];?><!--</th>-->
                        <!--                        --><?php //endfor;?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($hCount) AND !empty($_SESSION['viewHotel'])) for ($i=0; $i<$hCount;$i++):?>
                        <tr>
                            <td scope="col"><?php $j=1; echo $j=$j+$i?></td>
                            <td scope="col"><?php echo $_SESSION['viewHotel'][$i]['hotelName'];?></td>
                            <td scope="col"><?php echo $_SESSION['viewHotel'][$i]['hotelType'];?></td>
                            <td scope="col"><?php echo $_SESSION['viewHotel'][$i]['hotelRoomPrice'];?></td>
                            <td scope="col"><?php echo $_SESSION['viewHotel'][$i]['currentOffer'];?></td>
                        </tr>
                    <?php endfor;?>
                    </tbody>
                </table>

                <div class="container-login100-form-btn">
                    <a href="reset.php">
                        <button class="login100-form-btn" type="button">
                            Reset
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="assets/signup/vendor/jquery/jquery.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/bootstrap/assets/login/js/popper.js"></script>
<script src="assets/login/vendor/bootstrap/assets/login/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/tilt/tilt.jquery.min.js"></script>
<script src="assets/signup/vendor/datepicker/moment.min.js"></script>
<script src="assets/signup/vendor/datepicker/daterangepicker.js"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="assets/login/js/main.js"></script>
<!--===============================================================================================-->
<script src="assets/signup/js/global.js"></script>

</body>
</html>
