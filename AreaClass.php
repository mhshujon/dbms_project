<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/25/2018
 * Time: 11:49 PM
 */
include ('Connection.php');

class AreaClass extends Connection
{
    private $areaID = "NULL";
    private $areaName;
    private $touristSpots;

    public function set($data = array()){
        if (array_key_exists('areaID', $data)) {
            $this->areaID = $data['areaID'];
        }
        if (array_key_exists('areaName', $data)) {
            $this->areaName = $data['areaName'];
        }
        if (array_key_exists('touristSpots', $data)) {
            $this->touristSpots = $data['touristSpots'];
        }
////        var_dump($this);
//        return $this->usersEmail;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `area` (`areaID`, `areaName`, `touristSpots`) VALUES (:areaID, :areaName, :touristSpots)");
            $result =  $stmt->execute(array(
                ':areaID' => $this->areaID,
                ':areaName' => $this->areaName,
                ':touristSpots' => $this->touristSpots,
            ));

//            var_dump('this->usersEmail');
            echo "\nPDOStatement::errorInfo():\n";
            $arr = $stmt->errorInfo();
            print_r($arr);
            if($result){
//                $_SESSION['msg'] = 'Data successfully Inserted !!';
//                header('location:index.php');
//                echo $this->usersEmail;
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `area`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function count(){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `area`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function storyID($storyID){
        try{
            $stmt = $this->con->prepare("SELECT CONCAT(firstName, ' ', lastName)FROM `users` WHERE  `storyID`='$storyID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function user_validation($storyID, $pass){
        try{
            $stmt = $this->con->prepare("SELECT storyID, pass FROM `users` WHERE `storyID`='$storyID' AND `pass`='$pass'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function storyID_validation($storyID){
        try{
            $stmt = $this->con->prepare("SELECT storyID FROM `users` WHERE `storyID`='$storyID'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

//    public function view($id){
//        try{
//            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            return $stmt->fetch(PDO::FETCH_ASSOC);
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }
//
//    public function delete($id){
//        try{
//            $stmt = $this->con->prepare("DELETE FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('location:index.php');
//            }
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }

    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`users` SET `firstName` = :firstName, `lastName` = :lastName, `storyID` = :storyID, `usersEmail` = :usersEmail, `dob` = :dob, `gender` = :gender, `address` = :address WHERE `storyID` = :storyID;");

            $result =  $stmt->execute(array(
                ':firstName' => $this->firstName,
                ':lastName' => $this->lastName,
                ':storyID' => $this->storyID,
                ':usersEmail' => $this->usersEmail,
                ':dob' => $this->dob,
                ':gender' => $this->gender,
                ':address' => $this->address
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                echo 'Yes'.$this->storyID;
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}