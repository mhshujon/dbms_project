<?php
session_start();
if (isset($_SESSION['index'][0]['type'])){
    if ($_SESSION['index'][0]['type'] == 'admin'){
        include 'AreaClass.php';
        $object = new AreaClass();

        $_SESSION['areaIndex'] = $object->index();

        if (isset($_SESSION['areaIndex'])){
            $count = $object->count();
            $col = $count[0]['col'];
        }

        if (isset($_SESSION['eventStatus'])){
            if ($_SESSION['eventStatus'] == 'success'){
                echo "<script>window.alert('Event generated successfully.')</script>";
                $_SESSION['eventStatus'] = '';
            }
        }
    }
    else
        header('location: index.php');
}
else
    header('location: index.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>The Travellers</title>
    <base href="http://localhost/dbms_project/"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link href="assets/signup/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="assets/login/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100" style="background-image: url('assets/blog/story_form/images/bg-01.jpg');">
        <div class="wrap-login100">
            <!--            <div class="login100-pic js-tilt" data-tilt>-->
            <!--                <img src="assets/login/images/img-01.png" alt="IMG">-->
            <!--            </div>-->

            <form class="signup-form validate-form" method="POST" action="store_events.php">
					<span class="login100-form-title">
						Event Create
					</span>

                <div class="row row-space">
                    <div class="col-12">
                        <div class="wrap-input100 validate-input" data-validate = "Event tittle is required!">
                            <input class="input100" type="text" name="eventTittle" placeholder="Event Tittle">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row row-space">
<!--                    <div class="col-6">-->
<!--                        <div class="wrap-input100 validate-input" data-validate = "Hotel type is required!">-->
<!--                            <select class="input100" name="hotelType"  value="" style="border-style: hidden">-->
<!--                                <option selected disabled>Hotel Type</option>-->
<!--                                <option value="A">A Type</option>-->
<!--                                <option value="B" >B Type</option>-->
<!--                                <option value="C">C Type</option>-->
<!--                                <option value="D">D Type</option>-->
<!--                            </select>-->
<!--                            <span class="focus-input100"></span>-->
<!--                            <span class="symbol-input100">-->
<!--							    <i class="fa fa-transgender-alt" aria-hidden="true"></i>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="col-6">
                        <div class="wrap-input100 validate-input" data-validate = "This field is required!">
                            <input class="input100 input--style-1 js-datepicker" type="text" name="eventDateFrom" placeholder="Event date from">
                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-calendar-check" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="wrap-input100 validate-input" data-validate = "This field is required!">
                            <input class="input100 input--style-1 js-datepicker" type="text" name="eventDateTO" placeholder="Event date to">
                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-calendar-check" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row row-space">
                    <div class="col-6" data-validate = "This field is required!">
                        <div class="wrap-input100 validate-input">
                            <select class="input100" name="destinationFrom_areaID"  value="" style="border-style: hidden">
                                <option selected disabled>Location from</option>
                                <?php if(isset($col))for ($i=0; $i<$col; $i++):?>
                                    <option value="<?php echo $_SESSION['areaIndex'][$i]['areaID'];?>"><?php echo $_SESSION['areaIndex'][$i]['areaName'];?></option>
                                <?php endfor;?>
                            </select>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-transgender-alt" aria-hidden="true"></i>
                            </span>
                            <!--                            <div class="select-dropdown"></div>-->
                        </div>
                    </div>
                    <div class="col-6" data-validate = "Hotel area name is required!">
                        <div class="wrap-input100 validate-input">
                            <select class="input100" name="destinationTO_areaID"  value="" style="border-style: hidden">
                                <option selected disabled>Location TO</option>
                                <?php if(isset($col))for ($i=0; $i<$col; $i++):?>
                                    <option value="<?php echo $_SESSION['areaIndex'][$i]['areaID'];?>"><?php echo $_SESSION['areaIndex'][$i]['areaName'];?></option>
                                <?php endfor;?>
                            </select>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-transgender-alt" aria-hidden="true"></i>
                            </span>
                            <!--                            <div class="select-dropdown"></div>-->
                        </div>
                    </div>
                </div>
                <div class="row row-space">
                    <div class="col-6">
                        <div class="wrap-input100 validate-input" data-validate = "This field is required!">
                            <input class="input100" type="text" name="duration" placeholder="Tour duration">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="wrap-input100 validate-input" data-validate = "This field is required!">
                            <input class="input100" type="text" name="budget" placeholder="Tour budget">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
							    <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row row-space">
                    <div class="col-12">
                        <div class="wrap-input2 validate-input" data-validate = "This field is required!">
                            <textarea rows="8" class="input100" name="description" placeholder="FULL DESCRIPTION"></textarea>
                            <span class="focus-input2" data-placeholder="FULL DESCRIPTION"></span>
                        </div>
                    </div>
                </div>


                <div class="container-login100-form-btn">
                    <button class="login100-form-btn" type="submit">
                        Submit
                    </button>
                </div>
                <div class="container-login100-form-btn">
                    <a href="data_input.php">
                        <button class="login100-form-btn" type="button">
                            Cancel
                        </button>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="assets/signup/vendor/jquery/jquery.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/bootstrap/assets/login/js/popper.js"></script>
<script src="assets/login/vendor/bootstrap/assets/login/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/vendor/tilt/tilt.jquery.min.js"></script>
<script src="assets/signup/vendor/datepicker/moment.min.js"></script>
<script src="assets/signup/vendor/datepicker/daterangepicker.js"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="assets/login/js/main.js"></script>
<!--===============================================================================================-->
<script src="assets/signup/js/global.js"></script>

</body>
</html>
