<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/26/2018
 * Time: 1:55 PM
 */

include('Users.php');
session_start();

$email = $_POST['email'];
$pass = $_POST['pass'];
$object = new Users();
//var_dump($_POST);
$arr = $object->user_validation($email, $pass);

//var_dump($arr);

try{
    if(!empty($arr)){
//        echo "<script>window.location.assign('index.php')</script>";
        $_SESSION['login_status'] = 'success';
        $_SESSION['welcome'] = 'yes';
//        $name = $object->email($email);
//        $_SESSION['name'] = $name[0];
        $_SESSION['email'] = $email;
        $_SESSION['index'] = $object->index($email);
//        var_dump($_SESSION['index'][0]['type']);
        if (isset($_SESSION['index'][0]['type'])){
            if ($_SESSION['index'][0]['type'] == 'user')
                header('location: index.php');
            elseif ($_SESSION['index'][0]['type'] == 'admin')
                header('location: data_input.php');
        }
    }
    else{
        $_SESSION['login_status'] = 'invalid';
        header('location: login.php');
    }

}
catch(PDOException $ex){
    echo "<script>window.location.assign('login.php')</script>";
    $_SESSION['login_status'] = 'dberror';
}
