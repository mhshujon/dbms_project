<?php

session_start();

if (isset($_SESSION['index'])){
    if ($_SESSION['index'][0]['type'] == 'admin'){
        $name_1='';
        $name_2='';

        foreach ($_SESSION['index'] as $key=>$val){
            $value=$val;
        }
        if (!empty($value)){
            $name_1=$value['firstName'];
            $name_2=$value['lastName'];
        }

        if (isset($_SESSION['welcome'])){
            if ($_SESSION['welcome'] == 'yes'){
                $_SESSION['welcome'] = '';
                echo "<script>window.alert('Logged in successfully')</script>";
            }
        }
    }
    else
        header('location: index.php');
}
else
    header('location: login.php');

?>

<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>The Travellers</title>
    <base href="http://localhost/dbms_project/"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="assets/blog/css/base.css">
    <link rel="stylesheet" href="assets/blog/css/vendor.css">
    <link rel="stylesheet" href="assets/blog/css/main.css">

    <!-- script
    ================================================== -->
    <script src="assets/blog/js/modernizr.js"></script>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top">

<!-- preloader
================================================== -->
<div id="preloader">
    <div id="loader" class="dots-fade">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>


<!-- header
================================================== -->
<header class="s-header header">

    <div class="header__logo">
        <a class="logo" href="index.php">
            <img src="assets/blog/images/logo.svg" alt="Homepage">
        </a>
    </div> <!-- end header__logo-->

    <a class="header__search-trigger" ></a>
    <div class="header__search">

        <form role="search" method="get" class="header__search-form" action="#">
            <label>
                <span class="hide-content">Search for:</span>
                <input type="search" class="search-field" placeholder="Type Keywords" value="" name="s" title="Search for:" autocomplete="off">
            </label>
            <input type="submit" class="search-submit" value="Search">
        </form>

        <a  title="Close Search" class="header__overlay-close">Close</a>

    </div>  <!-- end header__search -->

    <a class="header__toggle-menu" href="#0" title="Menu"><span>Menu</span></a>
    <nav class="header__nav-wrap">

        <h2 class="header__nav-heading h6">Navigate to</h2>

        <ul class="header__nav">
            <li class="current"><a href="data_input.php" title="">Home</a></li>
            <li class="current"><a href="area.php" title="">Area</a></li>
            <li class="current"><a href="transport.php" title="">Transport</a></li>
            <li class="current"><a href="hotel.php" title="">Hotel</a></li>
            <li class="current"><a href="eventForm.php" title="">Event</a></li>
            <li class="current"><a href="logout.php" title="">Logout</a></li>
<!--            --><?php
//            if (isset($name_1) AND isset($name_2)){
//                echo '<h3>Hello '.'<strong>'.'<a href="view.php">'.'<h3>'.$name_1.' '.$name_2.'</h3>'.'</a>'.'</strong>'.'</h3>';
//            }
//            //            var_dump($_SESSION);
//            ?>
            <!--                <li><a href="story_form.php" title="">Write Your Story</a></li>-->
        </ul> <!-- end header__nav -->

        <a  title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>

    </nav> <!-- end header__nav-wrap -->

</header> <!-- s-header -->


<!-- Java Script
================================================== -->
<script src="assets/blog/js/jquery-3.2.1.min.js"></script>
<script src="assets/blog/js/plugins.js"></script>
<script src="assets/blog/js/main.js"></script>

</body>

</html>