<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/30/2018
 * Time: 5:07 AM
 */
include ('Connection.php');

class Event extends Connection
{
    private $eventID = NULL;
    private $eventTittle;
    private $eventDateFrom;
    private $eventDateTO;
    private $destinationFrom_areaID;
    private $destinationTO_areaID;
    private $duration;
    private $budget;
    private $description;
    private $image = NULL;

    public function set($data = array()){
        if (array_key_exists('eventID', $data)) {
            $this->eventID = $data['eventID'];
        }
        if (array_key_exists('eventTittle', $data)) {
            $this->eventTittle = $data['eventTittle'];
        }
        if (array_key_exists('eventDateFrom', $data)) {
            $this->eventDateFrom = $data['eventDateFrom'];
        }
        if (array_key_exists('eventDateTO', $data)) {
            $this->eventDateTO = $data['eventDateTO'];
        }
        if (array_key_exists('destinationFrom_areaID', $data)) {
            $this->destinationFrom_areaID = $data['destinationFrom_areaID'];
        }
        if (array_key_exists('destinationTO_areaID', $data)) {
            $this->destinationTO_areaID = $data['destinationTO_areaID'];
        }
        if (array_key_exists('duration', $data)) {
            $this->duration = $data['duration'];
        }
        if (array_key_exists('budget', $data)) {
            $this->budget = $data['budget'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }
////        var_dump($this);
//        return $this->eventDateTO;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `tour_events` (`eventID`, `eventTittle`, `eventDateFrom`, `eventDateTO`, `destinationFrom_areaID`, `destinationTO_areaID`, `duration`, `budget`, `description`, `image`) VALUES (:eventID, :eventTittle, :eventDateFrom, :eventDateTO, :destinationFrom_areaID, :destinationTO_areaID, :duration, :budget, :description, :image)");
            $result =  $stmt->execute(array(
                ':eventID' => $this->eventID,
                ':eventTittle' => $this->eventTittle,
                ':eventDateFrom' => $this->eventDateFrom,
                ':eventDateTO' => $this->eventDateTO,
                ':destinationFrom_areaID' => $this->destinationFrom_areaID,
                ':destinationTO_areaID' => $this->destinationTO_areaID,
                ':duration' => $this->duration,
                ':budget' => $this->budget,
                ':description' => $this->description,
                ':image' => $this->image
            ));

//            var_dump('this->eventDateTO');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                $_SESSION['msg'] = 'Data successfully Inserted !!';
//                header('location:index.php');
//                echo $this->eventDateTO;
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `tour_events` ORDER BY eventID DESC");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function count(){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `tour_events`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function eventDateFrom($eventDateFrom){
        try{
            $stmt = $this->con->prepare("SELECT CONCAT(eventID, ' ', eventTittle)FROM `tour_events` WHERE  `eventDateFrom`='$eventDateFrom'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function user_validation($eventDateFrom, $destinationFrom_areaID){
        try{
            $stmt = $this->con->prepare("SELECT eventDateFrom, destinationFrom_areaID FROM `tour_events` WHERE `eventDateFrom`='$eventDateFrom' AND `destinationFrom_areaID`='$destinationFrom_areaID'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function eventDateFrom_validation($eventDateFrom){
        try{
            $stmt = $this->con->prepare("SELECT eventDateFrom FROM `tour_events` WHERE `eventDateFrom`='$eventDateFrom'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
    public function eventFullView($id){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `tour_events` WHERE  `eventID`='$id'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
//    public function view($id){
//        try{
//            $stmt = $this->con->prepare("SELECT * FROM `tour_events` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            return $stmt->fetch(PDO::FETCH_ASSOC);
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }
//
//    public function delete($id){
//        try{
//            $stmt = $this->con->prepare("DELETE FROM `tour_events` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('location:index.php');
//            }
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }

    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`tour_events` SET `eventID` = :eventID, `eventTittle` = :eventTittle, `eventDateFrom` = :eventDateFrom, `eventDateTO` = :eventDateTO, `destinationTO_areaID` = :destinationTO_areaID, `duration` = :duration, `budget` = :budget WHERE `eventDateFrom` = :eventDateFrom;");

            $result =  $stmt->execute(array(
                ':eventID' => $this->eventID,
                ':eventTittle' => $this->eventTittle,
                ':eventDateFrom' => $this->eventDateFrom,
                ':eventDateTO' => $this->eventDateTO,
                ':destinationTO_areaID' => $this->destinationTO_areaID,
                ':duration' => $this->duration,
                ':budget' => $this->budget
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                echo 'Yes'.$this->eventDateFrom;
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}