<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/25/2018
 * Time: 11:49 PM
 */
include ('Connection.php');

class HotelClass extends Connection
{
    private $hotelID;
    private $hotelName;
    private $hotel_areaID;
    private $hotelID_TypePrice;
    private $hotelType;
    private $hotelRoomPrice;
    private $currentOffer;

    public function set($data = array()){
        if (array_key_exists('hotelID', $data)) {
            $this->hotelID = $data['hotelID'];
        }
        if (array_key_exists('hotelName', $data)) {
            $this->hotelName = $data['hotelName'];
        }
        if (array_key_exists('hotel_areaID', $data)) {
            $this->hotel_areaID = $data['hotel_areaID'];
        }
        if (array_key_exists('hotelID_TypePrice', $data)) {
            $this->hotelID_TypePrice = $data['hotelID_TypePrice'];
        }
        if (array_key_exists('hotelType', $data)) {
            $this->hotelType = $data['hotelType'];
        }
        if (array_key_exists('hotelRoomPrice', $data)) {
            $this->hotelRoomPrice = $data['hotelRoomPrice'];
        }
        if (array_key_exists('currentOffer', $data)) {
            $this->currentOffer = $data['currentOffer'];
        }
////        var_dump($this);
//        return $this->usersEmail;
    }

    public function store(){
        try{
            $stmt1 = $this->con->prepare("INSERT INTO `hotel` (`hotelID`, `hotelName`, `hotel_areaID`) VALUES (:hotelID, :hotelName, :hotel_areaID)");
            $stmt1->execute(array(
                ':hotelID' => $this->hotelID,
                ':hotelName' => $this->hotelName,
                ':hotel_areaID' => $this->hotel_areaID
            ));
            echo "\nPDOStatement::errorInfo():\n";
            $arr = $stmt1->errorInfo();
            print_r($arr);
            $stmt2 = $this->con->prepare("INSERT INTO `hoteltypeprice` (`hotelID_TypePrice`, `hotelType`, `hotelRoomPrice`, `currentOffer`) VALUES (:hotelID_TypePrice, :hotelType, :hotelRoomPrice, :currentOffer)");
            $stmt2->execute(array(
                ':hotelID_TypePrice' => $this->hotelID,
                ':hotelType' => $this->hotelType,
                ':hotelRoomPrice' => $this->hotelRoomPrice,
                ':currentOffer' => $this->currentOffer
            ));
            echo "\nPDOStatement::errorInfo():\n";
            $arr = $stmt2->errorInfo();
            print_r($arr);

//            var_dump('this->usersEmail');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt1->errorInfo();
//            print_r($arr);
//            if($result){
//                $_SESSION['msg'] = 'Data successfully Inserted !!';
//                header('location:index.php');
//                echo $this->usersEmail;
//            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `hotel`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function count(){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `hotel`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function storyID($storyID){
        try{
            $stmt = $this->con->prepare("SELECT CONCAT(firstName, ' ', lastName)FROM `users` WHERE  `storyID`='$storyID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function user_validation($storyID, $pass){
        try{
            $stmt = $this->con->prepare("SELECT storyID, pass FROM `users` WHERE `storyID`='$storyID' AND `pass`='$pass'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function storyID_validation($storyID){
        try{
            $stmt = $this->con->prepare("SELECT storyID FROM `users` WHERE `storyID`='$storyID'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

//    public function view($id){
//        try{
//            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            return $stmt->fetch(PDO::FETCH_ASSOC);
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }
//
//    public function delete($id){
//        try{
//            $stmt = $this->con->prepare("DELETE FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('location:index.php');
//            }
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }

    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`users` SET `firstName` = :firstName, `lastName` = :lastName, `storyID` = :storyID, `usersEmail` = :usersEmail, `dob` = :dob, `gender` = :gender, `address` = :address WHERE `storyID` = :storyID;");

            $result =  $stmt->execute(array(
                ':firstName' => $this->firstName,
                ':lastName' => $this->lastName,
                ':storyID' => $this->storyID,
                ':usersEmail' => $this->usersEmail,
                ':dob' => $this->dob,
                ':gender' => $this->gender,
                ':address' => $this->address
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                echo 'Yes'.$this->storyID;
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}