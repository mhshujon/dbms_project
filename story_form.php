<?php
session_start();
if (empty($_SESSION)){
    header('location: index.php');
}
else{
    include 'AreaClass.php';
    $object = new AreaClass();

    $_SESSION['areaIndex'] = $object->index();

    if (isset($_SESSION['areaIndex'])){
        $count = $object->count();
        $col = $count[0]['col'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>The Travellers</title>
    <base href="http://localhost/dbms_project/"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/blog/story_form/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/blog/story_form/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/blog/story_form/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/blog/story_form/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/blog/story_form/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/blog/story_form/css/util.css">
    <link rel="stylesheet" type="text/css" href="assets/blog/story_form/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="bg-contact2" style="background-image: url('assets/blog/story_form/images/bg-01.jpg');">
    <div class="container-contact2">
        <div class="wrap-contact2">

            <form class="needs-validation" style="text-align: center" method="POST" action="story_store.php" novalidate>
<!--                <div class="form-row">-->
<!--                    <div class="col-md-12 mb-3 md-form">-->
<!--                        <label for="validationCustom012">First name</label>-->
<!--                        <input type="text" class="form-control" id="validationCustom012" placeholder="First name" value=""-->
<!--                               required>-->
<!--                        <div class="valid-feedback">-->
<!--                            Looks good!-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <span class="contact2-form-title">
                    Write Your Story
                </span>
                <div class="form-row">
                    <div class="col-md-12 mb-3 md-form">
                        <label for="validationCustomUsername2">TITLE</label>
                        <input type="text" name="tittle" class="form-control" id="validationCustomUsername2" aria-describedby="inputGroupPrepend2"
                               required>
<!--                        <div class="invalid-feedback">-->
<!--                            Please choose a username.-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3 md-form">
                        <label for="validationCustomUsername2">Destination From</label>
                            <select class="browser-default custom-select form-control" name="destinationFrom_areaID" required>
                                <option value=""></option>
                                <?php if(isset($col))for ($i=0; $i<$col; $i++):?>
                                <option value="<?php echo $_SESSION['areaIndex'][$i]['areaID'];?>"><?php echo $_SESSION['areaIndex'][$i]['areaName'];?></option>
                                <?php endfor;?>
                            </select>
                    </div>
                    <div class="col-md-6 mb-3 md-form">
                        <label for="validationCustomUsername2">Destination To</label>
                        <select class="browser-default custom-select form-control" name="destinationTO_areaID" required>
                            <option value=""></option>
                            <?php if(isset($col))for ($i=0; $i<$col; $i++):?>
                            <option value="<?php echo $_SESSION['areaIndex'][$i]['areaID'];?>"><?php echo $_SESSION['areaIndex'][$i]['areaName'];?></option>
                            <?php endfor;?>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3 md-form">
                        <label for="validationCustomUsername2">Duration</label>
                        <input type="text" name="duration" class="form-control" id="validationCustomUsername2" aria-describedby="inputGroupPrepend2"
                               required>
<!--                        <div class="invalid-feedback">-->
<!--                            Please choose a username.-->
<!--                        </div>-->
                    </div>
                    <div class="col-md-4 mb-3 md-form">
                        <label for="validationCustomUsername2">Companion</label>
                        <input type="text" name="totalCompanion" class="form-control" id="validationCustomUsername2" aria-describedby="inputGroupPrepend2"
                               required>
<!--                        <div class="invalid-feedback">-->
<!--                            Please choose a username.-->
<!--                        </div>-->
                    </div>
                    <div class="col-md-4 mb-3 md-form">
                        <label for="validationCustomUsername2">Food Cost</label>
                        <input type="text" name="totalFoodCost" class="form-control" id="validationCustomUsername2" aria-describedby="inputGroupPrepend2"
                               required>
<!--                        <div class="invalid-feedback">-->
<!--                            Please choose a username.-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3 md-form">
                        <label for="validationCustomUsername2">Transport By</label>
                        <input type="text" name="transportBY" class="form-control" id="validationCustomUsername2" aria-describedby="inputGroupPrepend2"
                               required>
<!--                        <div class="invalid-feedback">-->
<!--                            Please choose a username.-->
<!--                        </div>-->
                    </div>
                    <div class="col-md-4 mb-3 md-form">
                        <label for="validationCustomUsername032">Transport Cost</label>
                        <input type="text" name="transportCost" class="form-control" id="validationCustomUsername3" aria-describedby="inputGroupPrepend3"
                               required>
<!--                        <div class="invalid-feedback">-->
<!--                            Please choose a.-->
<!--                        </div>-->
                    </div>
                    <div class="col-md-4 mb-3 md-form">
                        <label for="validationCustomUsername2">Total Cost</label>
                        <input type="text" name="totalCost" class="form-control" align="middle" id="validationCustomUsername2" aria-describedby="inputGroupPrepend2"
                               required>
<!--                        <div class="invalid-feedback">-->
<!--                            Please choose a username.-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="form-row">
                    <div class="wrap-input2">
                        <textarea rows="8" class="input2" name="description"></textarea>
                        <span class="focus-input2" data-placeholder="FULL DESCRIPTION"></span>
                    </div>
                </div>
<!--                <div class="form-group">-->
<!--                    <div class="form-check pl-0">-->
<!--                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required>-->
<!--                        <label class="form-check-label" for="invalidCheck2">-->
<!--                            Agree to terms and conditions-->
<!--                        </label>-->
<!--                        <div class="invalid-feedback">-->
<!--                            You must agree before submitting.-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <button class="btn btn-primary btn-rounded" type="submit">SHARE</button>
                <a href="blog.php">
                    <button class="btn btn-danger btn-rounded" type="button">CANCEL</button>
                </a>
            </form>

<!--            <form class="contact2-form needs-validation" method="POST" action="story_store.php" novalidate>-->
<!--                <div style="margin-top: -10px; margin-bottom: -10px">-->
<!--                    <span class="contact2-form-title">-->
<!--                    Write Your Story-->
<!--                </span>-->
<!---->
<!--                    <div class="wrap-input2 validate-input" data-validate="Tittle is required">-->
<!--                        <input class="input2" type="text" name="tittle" id="validationCustomUsername2" required>-->
<!--                        <span class="focus-input2" data-placeholder="TITTLE"></span>-->
<!--                        <div class="invalid-feedback">-->
<!--                            Please choose a username.-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="row row-space">-->
<!--                        <div class="col-6">-->
<!--                            <div class="wrap-input2 validate-input" data-validate = "Info. required">-->
<!--                                <input class="input2" type="text" name="destFrom">-->
<!--                                <span class="focus-input2" data-placeholder="DESTINATION FROM"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-6">-->
<!--                            <div class="wrap-input2 validate-input" data-validate = "Info. required">-->
<!--                                <input class="input2" type="text" name="destTO">-->
<!--                                <span class="focus-input2" data-placeholder="DESTINATION TO"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="row row-space">-->
<!--                        <div class="col-6">-->
<!--                            <div class="wrap-input2 validate-input" data-validate = "Info. required">-->
<!--                                <input class="input2" type="text" name="duration">-->
<!--                                <span class="focus-input2" data-placeholder="DURATION"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-6">-->
<!--                            <div class="wrap-input2 validate-input" data-validate = "Info. required">-->
<!--                                <input class="input2" type="text" name="transportBy">-->
<!--                                <span class="focus-input2" data-placeholder="TRANSPORT BY"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="row row-space">-->
<!--                        <div class="col-6">-->
<!--                            <div class="wrap-input2 validate-input" data-validate = "Info. required">-->
<!--                                <input class="input2" type="text" name="transportCost">-->
<!--                                <span class="focus-input2" data-placeholder="TRANSPORT COST"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-6">-->
<!--                            <div class="wrap-input2 validate-input" data-validate = "Info. required">-->
<!--                                <input class="input2" type="text" name="foodCost">-->
<!--                                <span class="focus-input2" data-placeholder="FOOD COST"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="row row-space">-->
<!--                        <div class="col-6">-->
<!--                            <div class="wrap-input2 validate-input" data-validate = "Info. required">-->
<!--                                <input class="input2" type="text" name="companion">-->
<!--                                <span class="focus-input2" data-placeholder="COMPANION"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-6">-->
<!--                            <div class="wrap-input2 validate-input" data-validate = "Info. required">-->
<!--                                <input class="input2" type="text" name="totalCost">-->
<!--                                <span class="focus-input2" data-placeholder="TOTAL COST"></span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!---->
<!--                    <div class="container-contact2-form-btn">-->
<!--                        <div class="wrap-contact2-form-btn">-->
<!--                            <div class="contact2-form-bgbtn"></div>-->
<!--                            <button class="contact2-form-btn" type="submit">-->
<!--                                SHARE-->
<!--                            </button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="assets/blog/story_form/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="assets/blog/story_form/vendor/bootstrap/js/popper.js"></script>
<script src="assets/blog/story_form/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="assets/blog/story_form/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="assets/login/js/main.js"></script>
<script src="assets/blog/story_form/js/main.js"></script>
<script src="assets/form-input-validation.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>

</body>
</html>
