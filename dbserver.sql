-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2019 at 09:46 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `areaID` int(11) NOT NULL,
  `areaName` varchar(20) NOT NULL,
  `touristSpots` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`areaID`, `areaName`, `touristSpots`) VALUES
(11, 'Dhaka', 'Ahsan Manjil, Lalbag Kella'),
(12, 'Chittagong', 'Foys Lake, Shitakundo'),
(13, 'Cox\'s Bazar', 'Himchori, Inani Beach, Ramu'),
(14, 'Bandorban', 'Nilgiri, Nilachol, Thanchi, Remakri'),
(15, 'Khagrachori', 'Hajachora Waterfalls, Risang Waterfalls, Alutila Guha'),
(16, 'Sylhet', 'Srimangal, Lalakhal, Ratargul, Bichanakandi'),
(17, 'Sajek', 'Konglak Para, Konglak Pahar, Ruilui Para');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `hotelID` int(11) NOT NULL,
  `hotelName` varchar(20) NOT NULL,
  `hotel_areaID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`hotelID`, `hotelName`, `hotel_areaID`) VALUES
(139120140, 'Nirvana Inn', 16),
(239838621, 'Nilachol Nilambori R', 14),
(272823238, 'Megh Machang', 17),
(396227349, 'Hotel Grand View', 16),
(488198196, 'Hotel Sonargaon', 11),
(535232503, 'Radisson', 12),
(550135568, 'The Peninsula', 12),
(646386500, 'Lushai Cottage', 17),
(1240959149, 'Hotel Cox Today', 13),
(1726477394, 'Long Beach', 13),
(1811364857, 'Hotel Shanghao', 15),
(1966989947, 'Hotel Green Castle', 15),
(1992662466, 'Hotel Hill View', 14),
(2134683476, 'Radisson', 11);

-- --------------------------------------------------------

--
-- Table structure for table `hoteltypeprice`
--

CREATE TABLE `hoteltypeprice` (
  `hotelID_TypePrice` int(11) NOT NULL,
  `hotelType` varchar(10) NOT NULL,
  `hotelRoomPrice` int(11) NOT NULL,
  `currentOffer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hoteltypeprice`
--

INSERT INTO `hoteltypeprice` (`hotelID_TypePrice`, `hotelType`, `hotelRoomPrice`, `currentOffer`) VALUES
(139120140, 'C', 3500, 5),
(239838621, 'B', 2500, 10),
(272823238, 'B', 3000, 0),
(396227349, 'B', 2500, 0),
(488198196, 'A', 10000, 0),
(535232503, 'A', 12000, 0),
(550135568, 'A', 4000, 5),
(646386500, 'C', 3000, 5),
(1240959149, 'A', 5000, 0),
(1726477394, 'A', 5000, 0),
(1811364857, 'D', 4000, 10),
(1966989947, 'C', 4000, 10),
(1992662466, 'B', 2000, 10),
(2134683476, 'A', 15000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE `stories` (
  `storyID` int(11) NOT NULL,
  `usersEmail` varchar(50) NOT NULL,
  `tittle` varchar(255) NOT NULL,
  `destinationFrom_areaID` int(11) NOT NULL,
  `destinationTO_areaID` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `transportBY` char(10) NOT NULL,
  `transportCost` int(11) NOT NULL,
  `totalFoodCost` int(11) NOT NULL,
  `totalCompanion` int(11) NOT NULL,
  `totalCost` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`storyID`, `usersEmail`, `tittle`, `destinationFrom_areaID`, `destinationTO_areaID`, `duration`, `transportBY`, `transportCost`, `totalFoodCost`, `totalCompanion`, `totalCost`, `description`, `image`, `date`) VALUES
(33, 'farhan@email.com', 'Queen Of Nature - Sajek', 11, 17, 4, 'Bus', 1500, 1500, 6, 5000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'NULL', '2019-01-11'),
(35, 'alif@email.com', 'Bandorban - Queen Of Nature', 11, 14, 3, 'Bus', 1200, 1000, 6, 4000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'NULL', '2019-01-11');

-- --------------------------------------------------------

--
-- Table structure for table `tour_events`
--

CREATE TABLE `tour_events` (
  `eventID` int(11) NOT NULL,
  `eventTittle` varchar(255) NOT NULL,
  `eventDateFrom` varchar(255) NOT NULL,
  `eventDateTO` varchar(255) NOT NULL,
  `destinationFrom_areaID` int(11) NOT NULL,
  `destinationTO_areaID` int(11) NOT NULL,
  `duration` varchar(50) NOT NULL,
  `budget` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tour_events`
--

INSERT INTO `tour_events` (`eventID`, `eventTittle`, `eventDateFrom`, `eventDateTO`, `destinationFrom_areaID`, `destinationTO_areaID`, `duration`, `budget`, `description`, `image`) VALUES
(9, 'Sajek - The Queen Of Nature', '17/01/2019', '21/01/2019', 11, 17, '4 Nights 3Days', 5000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', NULL),
(11, 'The Queen Of Nature - Bandorban', '06/02/2019', '11/02/2019', 11, 14, '3 Nights 2Days', 5000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transportation`
--

CREATE TABLE `transportation` (
  `transportID` int(11) NOT NULL,
  `transportName` varchar(20) NOT NULL,
  `transportCategory` varchar(10) NOT NULL,
  `transport_areaIDFrom` int(11) NOT NULL,
  `transport_areaIDTO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transportation`
--

INSERT INTO `transportation` (`transportID`, `transportName`, `transportCategory`, `transport_areaIDFrom`, `transport_areaIDTO`) VALUES
(49945939, 'Shyamoli', 'BUS', 15, 12),
(93384550, 'Shyamoli', 'BUS', 11, 15),
(144087733, 'Shyamoli', 'BUS', 11, 15),
(189996126, 'Hanif', 'BUS', 11, 13),
(284149047, 'Shyamoli', 'BUS', 15, 11),
(319828377, 'Shyamoli', 'BUS', 16, 11),
(353049732, 'Shubarna Express', 'Train', 16, 11),
(466356961, 'HANIF', 'BUS', 11, 12),
(487805908, 'Shyamoli', 'BUS', 14, 13),
(526426040, 'Hanif', 'BUS', 11, 15),
(535561545, 'Shyamoli', 'BUS', 14, 11),
(605035169, 'Chander Gari', 'Private Ca', 15, 16),
(629820537, 'Mahanagar Godhuli', 'Train', 11, 12),
(677233286, 'Hanif', 'BUS', 13, 11),
(703586110, 'Shyamoli', 'BUS', 14, 12),
(720808562, 'Hanif', 'BUS', 11, 14),
(856360036, 'HANIF', 'BUS', 11, 12),
(884710919, 'Hanif', 'BUS', 12, 15),
(890888819, 'Shyamoli', 'BUS', 11, 12),
(914607837, 'Hanif', 'BUS', 15, 11),
(950292611, 'Hanif', 'BUS', 12, 11),
(956619594, 'Shyamoli', 'BUS', 13, 11),
(1006859468, 'Mahanagar Godhuli', 'Train', 16, 11),
(1112256394, 'Hanif', 'BUS', 15, 11),
(1132376113, 'Shyamoli', 'BUS', 14, 11),
(1185662639, 'Hanif', 'BUS', 16, 11),
(1237761994, 'Hanif', 'BUS', 12, 11),
(1268049847, 'Chander Gari', 'Private Ca', 17, 15),
(1302260476, 'Shyamoli', 'BUS', 12, 13),
(1326716526, 'Shyamoli', 'BUS', 11, 12),
(1356669625, 'Hanif', 'BUS', 13, 12),
(1357629237, 'Mahanagar Godhuli', 'Train', 12, 11),
(1378284114, 'Shubarna Express', 'Train', 11, 12),
(1416366621, 'Shyamoli', 'BUS', 11, 13),
(1420076375, 'Hanif', 'BUS', 12, 14),
(1424794874, 'Shyamoli', 'BUS', 11, 14),
(1434031514, 'Hanif', 'BUS', 11, 13),
(1467357263, 'Shyamoli', 'BUS', 12, 15),
(1491241596, 'Shubarna Express', 'Train', 12, 11),
(1513311369, 'Hanif', 'BUS', 14, 11),
(1546810852, 'Shyamoli', 'BUS', 13, 14),
(1549275910, 'Hanif', 'BUS', 15, 12),
(1551873473, 'Hanif', 'BUS', 13, 14),
(1577212363, 'Hanif', 'BUS', 16, 11),
(1666263241, 'Shyamoli', 'BUS', 16, 12),
(1721934978, 'Shyamoli', 'BUS', 12, 14),
(1779640397, 'Shyamoli', 'BUS', 15, 11),
(1823177549, 'Shyamoli', 'BUS', 13, 12),
(1849621149, 'Shyamoli', 'BUS', 12, 11),
(1865885769, 'Hanif', 'BUS', 13, 11),
(1890189898, 'Hanif', 'BUS', 11, 15),
(1925958349, 'Shyamoli', 'BUS', 16, 11),
(1955549178, 'Hanif', 'BUS', 16, 12),
(1959102541, 'Shyamoli', 'BUS', 12, 11),
(1992326080, 'Hanif', 'BUS', 12, 13),
(2004167916, 'Hanif', 'BUS', 14, 13),
(2048057193, 'Shyamoli', 'BUS', 15, 11),
(2071592957, 'Hanif', 'BUS', 14, 11),
(2077353287, 'Hanif', 'BUS', 11, 14),
(2112859914, 'Shyamoli', 'BUS', 13, 11),
(2130439003, 'Hanif', 'BUS', 14, 12);

-- --------------------------------------------------------

--
-- Table structure for table `transport_categorytypeprice`
--

CREATE TABLE `transport_categorytypeprice` (
  `transportID_categoryTypePrice` int(11) NOT NULL,
  `transportCategoryType` varchar(10) NOT NULL,
  `transportCategoryTypePrice` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transport_categorytypeprice`
--

INSERT INTO `transport_categorytypeprice` (`transportID_categoryTypePrice`, `transportCategoryType`, `transportCategoryTypePrice`) VALUES
(49945939, 'NON-AC', '300'),
(93384550, 'NON-AC', '560'),
(144087733, 'AC', '1000'),
(189996126, 'AC', '1500'),
(284149047, 'AC', '1000'),
(319828377, 'AC', '1200'),
(353049732, 'AC', '500'),
(466356961, 'AC', '1000'),
(487805908, 'NON-AC', '300'),
(526426040, 'NON-AC', '560'),
(535561545, 'NON-AC', '1000'),
(605035169, 'NON-AC', '7500'),
(629820537, 'AC', '673'),
(677233286, 'AC', '1500'),
(703586110, 'NON-AC', '400'),
(720808562, 'NON-AC', '1000'),
(856360036, 'NON-AC', '550'),
(884710919, 'NON-AC', '400'),
(890888819, 'AC', '1000'),
(914607837, 'AC', '1000'),
(950292611, 'NON-AC', '550'),
(956619594, 'NON-AC', '1000'),
(1006859468, 'NON-AC', '355'),
(1112256394, 'NON-AC', '560'),
(1132376113, 'AC', '1500'),
(1185662639, 'NON-AC', '450'),
(1237761994, 'AC', '1500'),
(1268049847, 'NON-AC', '7500'),
(1302260476, 'NON-AC', '300'),
(1326716526, 'NON-AC', '550'),
(1356669625, 'NON-AC', '300'),
(1357629237, 'AC', '673'),
(1378284114, 'NON-AC', '355'),
(1416366621, 'AC', '1500'),
(1420076375, 'NON-AC', '400'),
(1424794874, 'AC', '1500'),
(1434031514, 'NON-AC', '800'),
(1467357263, 'NON-AC', '400'),
(1491241596, 'NON-AC', '355'),
(1513311369, 'AC', '1500'),
(1546810852, 'NON-AC', '400'),
(1549275910, 'NON-AC', '300'),
(1551873473, 'NON-AC', '400'),
(1577212363, 'AC', '1200'),
(1666263241, 'NON-AC', '400'),
(1721934978, 'NON-AC', '400'),
(1779640397, 'AC', '1000'),
(1823177549, 'NON-AC', '300'),
(1849621149, 'NON-AC', '550'),
(1865885769, 'NON-AC', '1000'),
(1890189898, 'AC', '1200'),
(1925958349, 'NON-AC', '450'),
(1955549178, 'NON-AC', '400'),
(1959102541, 'AC', '1500'),
(1992326080, 'NON-AC', '300'),
(2004167916, 'NON-AC', '300'),
(2048057193, 'NON-AC', '560'),
(2071592957, 'NON-AC', '1000'),
(2077353287, 'AC', '1500'),
(2112859914, 'AC', '1500'),
(2130439003, 'NON-AC', '400');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `type` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `gender` char(10) NOT NULL,
  `dob` char(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `mobileNumber` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`type`, `email`, `pass`, `firstName`, `lastName`, `gender`, `dob`, `address`, `mobileNumber`) VALUES
('user', 'alif@email.com', '123', 'Al Amin', 'Alif', 'Male', '02/01/2019', 'Azimpur', 1784896527),
('user', 'farhan@email.com', '123', 'Farhan', 'Ahmed', 'Male', '05/04/1995', 'Hajaribag', 1955673564),
('admin', 'monir@email.com', '123', 'Md Monir', 'Hossain', 'Male', '15/09/1995', 'Lalbagh', 1683852548);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`areaID`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`hotelID`),
  ADD KEY `fk_hotel_areaID` (`hotel_areaID`);

--
-- Indexes for table `hoteltypeprice`
--
ALTER TABLE `hoteltypeprice`
  ADD PRIMARY KEY (`hotelID_TypePrice`,`hotelType`);

--
-- Indexes for table `stories`
--
ALTER TABLE `stories`
  ADD PRIMARY KEY (`storyID`,`usersEmail`),
  ADD KEY `fk_usersEmail` (`usersEmail`),
  ADD KEY `fk_destinationFrom_areaID` (`destinationFrom_areaID`),
  ADD KEY `fk_destinationTO_areaID` (`destinationTO_areaID`);

--
-- Indexes for table `tour_events`
--
ALTER TABLE `tour_events`
  ADD PRIMARY KEY (`eventID`),
  ADD KEY `fk_tour_events_destinationFrom_areaID` (`destinationFrom_areaID`),
  ADD KEY `fk_tour_events_destinationTO_areaID` (`destinationTO_areaID`);

--
-- Indexes for table `transportation`
--
ALTER TABLE `transportation`
  ADD PRIMARY KEY (`transportID`),
  ADD KEY `fk_transport_areaIDFrom` (`transport_areaIDFrom`),
  ADD KEY `fk_transport_areaIDTO` (`transport_areaIDTO`);

--
-- Indexes for table `transport_categorytypeprice`
--
ALTER TABLE `transport_categorytypeprice`
  ADD PRIMARY KEY (`transportID_categoryTypePrice`,`transportCategoryType`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `areaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `hotelID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2134683477;

--
-- AUTO_INCREMENT for table `stories`
--
ALTER TABLE `stories`
  MODIFY `storyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tour_events`
--
ALTER TABLE `tour_events`
  MODIFY `eventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `transportation`
--
ALTER TABLE `transportation`
  MODIFY `transportID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2130439004;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `fk_hotel_areaID` FOREIGN KEY (`hotel_areaID`) REFERENCES `area` (`areaID`);

--
-- Constraints for table `hoteltypeprice`
--
ALTER TABLE `hoteltypeprice`
  ADD CONSTRAINT `fk_hotelID_TypePrice` FOREIGN KEY (`hotelID_TypePrice`) REFERENCES `hotel` (`hotelID`);

--
-- Constraints for table `stories`
--
ALTER TABLE `stories`
  ADD CONSTRAINT `fk_destinationFrom_areaID` FOREIGN KEY (`destinationFrom_areaID`) REFERENCES `area` (`areaID`),
  ADD CONSTRAINT `fk_destinationTO_areaID` FOREIGN KEY (`destinationTO_areaID`) REFERENCES `area` (`areaID`),
  ADD CONSTRAINT `fk_usersEmail` FOREIGN KEY (`usersEmail`) REFERENCES `users` (`email`);

--
-- Constraints for table `tour_events`
--
ALTER TABLE `tour_events`
  ADD CONSTRAINT `fk_tour_events_destinationFrom_areaID` FOREIGN KEY (`destinationFrom_areaID`) REFERENCES `area` (`areaID`),
  ADD CONSTRAINT `fk_tour_events_destinationTO_areaID` FOREIGN KEY (`destinationTO_areaID`) REFERENCES `area` (`areaID`);

--
-- Constraints for table `transportation`
--
ALTER TABLE `transportation`
  ADD CONSTRAINT `fk_transport_areaIDFrom` FOREIGN KEY (`transport_areaIDFrom`) REFERENCES `area` (`areaID`),
  ADD CONSTRAINT `fk_transport_areaIDTO` FOREIGN KEY (`transport_areaIDTO`) REFERENCES `area` (`areaID`);

--
-- Constraints for table `transport_categorytypeprice`
--
ALTER TABLE `transport_categorytypeprice`
  ADD CONSTRAINT `fk_trasportID_categoryTypePrice` FOREIGN KEY (`transportID_categoryTypePrice`) REFERENCES `transportation` (`transportID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
