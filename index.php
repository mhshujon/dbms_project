<?php
include_once 'header.php'
?>

<!-- about
================================================== -->
<section id='about' class="s-about">

    <div class="row section-header has-bottom-sep" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead subhead--dark">Hello There</h3>
            <h1 class="display-1 display-1--light">We Are The Travellers</h1>
        </div>
    </div> <!-- end section-header -->

    <div class="row about-desc" data-aos="fade-up">
        <div class="col-full">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.
            </p>
        </div>
    </div> <!-- end about-desc -->

    <div class="row about-stats stats block-1-8 block-m-1-4 block-mob-full" data-aos="fade-up">

<!--        <div class="col-block stats__col ">-->
<!--            <div class="stats__count">127</div>-->
<!--            <h5>Awards Received</h5>-->
<!--        </div>-->
        <div id="events">
            <div class="col-block stats__col">
                <div class="stats__count">24</div>
                <h5>Events Completed</h5>
            </div>
            <div class="col-block stats__col">
                <div class="stats__count">15</div>
                <h5>Upcoming Events</h5>
            </div>
        </div>
<!--        <div class="col-block stats__col">-->
<!--            <div class="stats__count">102</div>-->
<!--            <h5>Happy Clients</h5>-->
<!--        </div>-->

    </div> <!-- end about-stats -->

    <div class="about__line"></div>

</section> <!-- end s-about -->


<!-- services
================================================== -->
<section id='services' class="s-services">

    <div class="row section-header has-bottom-sep" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead">What We Do</h3>
            <h1 class="display-2">We’ve got everything you need to make your tours easy to plan and comfortable.</h1>
        </div>
    </div> <!-- end section-header -->

    <div class="row services-list block-1-2 block-tab-full">

        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon">
                <i class="icon-paint-brush"></i>
            </div>
            <div class="service-text">
                <h3 class="h2">Tourist Spots</h3>
                <p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                </p>
            </div>
        </div>

        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon">
                <i class="icon-group"></i>
            </div>
            <div class="service-text">
                <h3 class="h2">Transportation Info</h3>
                <p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                </p>
            </div>
        </div>

        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon">
                <i class="icon-megaphone"></i>
            </div>
            <div class="service-text">
                <h3 class="h2">Hotel Info</h3>
                <p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                </p>
            </div>
        </div>

        <div class="col-block service-item" data-aos="fade-up">
            <div class="service-icon">
                <i class="icon-earth"></i>
            </div>
            <div class="service-text">
                <h3 class="h2">Budget Tour Plan</h3>
                <p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.
                </p>
            </div>
        </div>

<!--        <div class="col-block service-item" data-aos="fade-up">-->
<!--            <div class="service-icon">-->
<!--                <i class="icon-cube"></i>-->
<!--            </div>-->
<!--            <div class="service-text">-->
<!--                <h3 class="h2">Packaging Design</h3>-->
<!--                <p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.-->
<!--                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.-->
<!--                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.-->
<!--                </p>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="col-block service-item" data-aos="fade-up">-->
<!--            <div class="service-icon"><i class="icon-lego-block"></i></div>-->
<!--            <div class="service-text">-->
<!--                <h3 class="h2">Web Development</h3>-->
<!--                <p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.-->
<!--                    Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.-->
<!--                    Sunt suscipit voluptas ipsa in tempora esse soluta sint.-->
<!--                </p>-->
<!--            </div>-->
<!--        </div>-->

    </div> <!-- end services-list -->

</section> <!-- end s-services -->


<!-- works
================================================== -->
<section id='works' class="s-works">

    <div class="intro-wrap">

        <div class="row section-header has-bottom-sep light-sep" data-aos="fade-up">
            <div class="col-full">
                <h3 class="subhead">Recent Works</h3>
                <h1 class="display-2 display-2--light">We love what we do, check out some of our latest works</h1>
            </div>
        </div> <!-- end section-header -->

    </div> <!-- end intro-wrap -->

    <div class="row works-content">
        <div class="col-full masonry-wrap">
            <div class="masonry">

                <div class="masonry__brick" data-aos="fade-up">
                    <div class="item-folio">

                        <div class="item-folio__thumb">
                            <a href="assets/images/portfolio/gallery/g-shutterbug.jpg" class="thumb-link" title="Shutterbug" data-size="1050x700">
                                <img src="assets/images/portfolio/lady-shutterbug.jpg"
                                     srcset="assets/images/portfolio/lady-shutterbug.jpg 1x, assets/images/portfolio/lady-shutterbug@2x.jpg 2x" alt="">
                            </a>
                        </div>

                        <div class="item-folio__text">
                            <h3 class="item-folio__title">
                                Shutterbug
                            </h3>
                            <p class="item-folio__cat">
                                Branding
                            </p>
                        </div>

                        <a href="https://www.behance.net/" class="item-folio__project-link" title="Project link">
                            <i class="icon-link"></i>
                        </a>

                        <div class="item-folio__caption">
                            <p>Vero molestiae sed aut natus excepturi. Et tempora numquam. Temporibus iusto quo.Unde dolorem corrupti neque nisi.</p>
                        </div>

                    </div>
                </div> <!-- end masonry__brick -->

                <div class="masonry__brick" data-aos="fade-up">
                    <div class="item-folio">

                        <div class="item-folio__thumb">
                            <a href="assets/images/portfolio/gallery/g-woodcraft.jpg" class="thumb-link" title="Woodcraft" data-size="1050x700">
                                <img src="assets/images/portfolio/woodcraft.jpg"
                                     srcset="assets/images/portfolio/woodcraft.jpg 1x, assets/images/portfolio/woodcraft@2x.jpg 2x" alt="">
                            </a>
                        </div>

                        <div class="item-folio__text">
                            <h3 class="item-folio__title">
                                Woodcraft
                            </h3>
                            <p class="item-folio__cat">
                                Web Design
                            </p>
                        </div>

                        <a href="https://www.behance.net/" class="item-folio__project-link" title="Project link">
                            <i class="icon-link"></i>
                        </a>

                        <div class="item-folio__caption">
                            <p>Vero molestiae sed aut natus excepturi. Et tempora numquam. Temporibus iusto quo.Unde dolorem corrupti neque nisi.</p>
                        </div>

                    </div>
                </div> <!-- end masonry__brick -->

                <div class="masonry__brick" data-aos="fade-up">
                    <div class="item-folio">

                        <div class="item-folio__thumb">
                            <a href="assets/images/portfolio/gallery/g-beetle.jpg" class="thumb-link" title="The Beetle Car" data-size="1050x700">
                                <img src="assets/images/portfolio/the-beetle.jpg"
                                     srcset="assets/images/portfolio/the-beetle.jpg 1x, assets/images/portfolio/the-beetle@2x.jpg 2x" alt="">
                            </a>
                        </div>

                        <div class="item-folio__text">
                            <h3 class="item-folio__title">
                                The Beetle
                            </h3>
                            <p class="item-folio__cat">
                                Web Development
                            </p>
                        </div>

                        <a href="https://www.behance.net/" class="item-folio__project-link" title="Project link">
                            <i class="icon-link"></i>
                        </a>

                        <div class="item-folio__caption">
                            <p>Vero molestiae sed aut natus excepturi. Et tempora numquam. Temporibus iusto quo.Unde dolorem corrupti neque nisi.</p>
                        </div>

                    </div>
                </div> <!-- end masonry__brick -->

                <div class="masonry__brick" data-aos="fade-up">
                    <div class="item-folio">

                        <div class="item-folio__thumb">
                            <a href="assets/images/portfolio/gallery/g-grow-green.jpg" class="thumb-link" title="Grow Green" data-size="1050x700">
                                <img src="assets/images/portfolio/grow-green.jpg"
                                     srcset="assets/images/portfolio/grow-green.jpg 1x, assets/images/portfolio/grow-green@2x.jpg 2x" alt="">
                            </a>
                        </div>

                        <div class="item-folio__text">
                            <h3 class="item-folio__title">
                                Grow Green
                            </h3>
                            <p class="item-folio__cat">
                                Branding
                            </p>
                        </div>

                        <a href="https://www.behance.net/" class="item-folio__project-link" title="Project link">
                            <i class="icon-link"></i>
                        </a>

                        <div class="item-folio__caption">
                            <p>Vero molestiae sed aut natus excepturi. Et tempora numquam. Temporibus iusto quo.Unde dolorem corrupti neque nisi.</p>
                        </div>

                    </div>
                </div> <!-- end masonry__brick -->

                <div class="masonry__brick" data-aos="fade-up">
                    <div class="item-folio">

                        <div class="item-folio__thumb">
                            <a href="assets/images/portfolio/gallery/g-guitarist.jpg" class="thumb-link" title="Guitarist" data-size="1050x700">
                                <img src="assets/images/portfolio/guitarist.jpg"
                                     srcset="assets/images/portfolio/guitarist.jpg 1x, assets/images/portfolio/guitarist@2x.jpg 2x" alt="">
                            </a>
                        </div>

                        <div class="item-folio__text">
                            <h3 class="item-folio__title">
                                Guitarist
                            </h3>
                            <p class="item-folio__cat">
                                Web Design
                            </p>
                        </div>

                        <a href="https://www.behance.net/" class="item-folio__project-link" title="Project link">
                            <i class="icon-link"></i>
                        </a>

                        <div class="item-folio__caption">
                            <p>Vero molestiae sed aut natus excepturi. Et tempora numquam. Temporibus iusto quo.Unde dolorem corrupti neque nisi.</p>
                        </div>

                    </div>
                </div> <!-- end masonry__brick -->

                <div class="masonry__brick" data-aos="fade-up">
                    <div class="item-folio">

                        <div class="item-folio__thumb">
                            <a href="assets/images/portfolio/gallery/g-palmeira.jpg" class="thumb-link" title="Palmeira" data-size="1050x700">
                                <img src="assets/images/portfolio/palmeira.jpg"
                                     srcset="assets/images/portfolio/palmeira.jpg 1x, assets/images/portfolio/palmeira@2x.jpg 2x" alt="">
                            </a>
                        </div>

                        <div class="item-folio__text">
                            <h3 class="item-folio__title">
                                Palmeira
                            </h3>
                            <p class="item-folio__cat">
                                Web Design
                            </p>
                        </div>

                        <a href="https://www.behance.net/" class="item-folio__project-link" title="Project link">
                            <i class="icon-link"></i>
                        </a>

                        <div class="item-folio__caption">
                            <p>Vero molestiae sed aut natus excepturi. Et tempora numquam. Temporibus iusto quo.Unde dolorem corrupti neque nisi.</p>
                        </div>

                    </div>
                </div> <!-- end masonry__brick -->

            </div> <!-- end masonry -->
        </div> <!-- end col-full -->
    </div> <!-- end works-content -->

</section> <!-- end s-works -->


<!-- clients
================================================== -->
<section id="clients" class="s-clients">

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead">Our Clients</h3>
            <h1 class="display-2">The Travellers is proudly
                partnered up with these clients</h1>
        </div>
    </div> <!-- end section-header -->

    <div class="row clients-outer" data-aos="fade-up">
        <div class="col-full">
            <div class="clients">

                <a href="#0" title="" class="clients__slide"><img src="assets/images/clients/apple.png" /></a>
                <a href="#0" title="" class="clients__slide"><img src="assets/images/clients/atom.png" /></a>
                <a href="#0" title="" class="clients__slide"><img src="assets/images/clients/blackberry.png" /></a>
                <a href="#0" title="" class="clients__slide"><img src="assets/images/clients/dropbox.png" /></a>
                <a href="#0" title="" class="clients__slide"><img src="assets/images/clients/envato.png" /></a>
                <a href="#0" title="" class="clients__slide"><img src="assets/images/clients/firefox.png" /></a>
                <a href="#0" title="" class="clients__slide"><img src="assets/images/clients/joomla.png" /></a>
                <a href="#0" title="" class="clients__slide"><img src="assets/images/clients/magento.png" /></a>

            </div> <!-- end clients -->
        </div> <!-- end col-full -->
    </div> <!-- end clients-outer -->

    <div class="row clients-testimonials" data-aos="fade-up">
        <div class="col-full">
            <div class="testimonials">

                <div class="testimonials__slide">

                    <p>Qui ipsam temporibus quisquam vel. Maiores eos cumque distinctio nam accusantium ipsum.
                        Laudantium quia consequatur molestias delectus culpa facere hic dolores aperiam. Accusantium quos qui praesentium corpori.
                        Excepturi nam cupiditate culpa doloremque deleniti repellat.</p>

                    <img src="assets/images/avatars/user-01.jpg" alt="Author image" class="testimonials__avatar">
                    <div class="testimonials__info">
                        <span class="testimonials__name">Tim Cook</span>
                        <span class="testimonials__pos">CEO, Apple</span>
                    </div>

                </div>

                <div class="testimonials__slide">

                    <p>Excepturi nam cupiditate culpa doloremque deleniti repellat. Veniam quos repellat voluptas animi adipisci.
                        Nisi eaque consequatur. Quasi voluptas eius distinctio. Atque eos maxime. Qui ipsam temporibus quisquam vel.</p>

                    <img src="assets/images/avatars/user-05.jpg" alt="Author image" class="testimonials__avatar">
                    <div class="testimonials__info">
                        <span class="testimonials__name">Sundar Pichai</span>
                        <span class="testimonials__pos">CEO, Google</span>
                    </div>

                </div>

                <div class="testimonials__slide">

                    <p>Repellat dignissimos libero. Qui sed at corrupti expedita voluptas odit. Nihil ea quia nesciunt. Ducimus aut sed ipsam.
                        Autem eaque officia cum exercitationem sunt voluptatum accusamus. Quasi voluptas eius distinctio.</p>

                    <img src="assets/images/avatars/user-02.jpg" alt="Author image" class="testimonials__avatar">
                    <div class="testimonials__info">
                        <span class="testimonials__name">Satya Nadella</span>
                        <span class="testimonials__pos">CEO, Microsoft</span>
                    </div>

                </div>

            </div><!-- end testimonials -->

        </div> <!-- end col-full -->
    </div> <!-- end client-testimonials -->

</section> <!-- end s-clients -->


<!-- contact
================================================== -->
<section id="contact" class="s-contact">

    <div class="overlay"></div>
    <div class="contact__line"></div>

    <div class="row section-header" data-aos="fade-up">
        <div class="col-full">
            <h3 class="subhead">Contact Us</h3>
            <h1 class="display-2 display-2--light">Reach out for our upcoming events or just say hello</h1>
        </div>
    </div>

    <div class="row contact-content" data-aos="fade-up">

        <div class="contact-primary">

            <h3 class="h6">Send Us A Message</h3>

            <form name="contactForm" id="contactForm" method="post" action="" novalidate="novalidate">
                <fieldset>

                    <div class="form-field">
                        <input name="contactName" type="text" id="contactName" placeholder="Your Name" value="" minlength="2" required="" aria-required="true" class="full-width">
                    </div>
                    <div class="form-field">
                        <input name="contactEmail" type="email" id="contactEmail" placeholder="Your Email" value="" required="" aria-required="true" class="full-width">
                    </div>
                    <div class="form-field">
                        <input name="contactSubject" type="text" id="contactSubject" placeholder="Subject" value="" class="full-width">
                    </div>
                    <div class="form-field">
                        <textarea name="contactMessage" id="contactMessage" placeholder="Your Message" rows="10" cols="50" required="" aria-required="true" class="full-width"></textarea>
                    </div>
                    <div class="form-field">
                        <button class="full-width btn--primary">Submit</button>
                        <div class="submit-loader">
                            <div class="text-loader">Sending...</div>
                            <div class="s-loader">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                        </div>
                    </div>

                </fieldset>
            </form>

            <!-- contact-warning -->
            <div class="message-warning">
                Something went wrong. Please try again.
            </div>

            <!-- contact-success -->
            <div class="message-success">
                Your message was sent, thank you!<br>
            </div>

        </div> <!-- end contact-primary -->

        <div class="contact-secondary">
            <div class="contact-info">

                <h3 class="h6 hide-on-fullwidth">Contact Info</h3>

                <div class="cinfo">
                    <h5>Where to Find Us</h5>
                    <p>
                        1600 Amphitheatre Parkway<br>
                        Mountain View, CA<br>
                        94043 US
                    </p>
                </div>

                <div class="cinfo">
                    <h5>Email Us At</h5>
                    <p>
                        contact@glintsite.com<br>
                        info@glintsite.com
                    </p>
                </div>

                <div class="cinfo">
                    <h5>Call Us At</h5>
                    <p>
                        Phone: (+63) 555 1212<br>
                        Mobile: (+63) 555 0100<br>
                        Fax: (+63) 555 0101
                    </p>
                </div>

                <ul class="contact-social">
                    <li>
                        <a href="https://www.facebook.com/mhshujon07"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/mh_shujon"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/mh_shujon"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="https://gitlab.com/mhshujon"><i class="fa fa-gitlab"></i></a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/powerplaygaming"><i class="fa fa-youtube-play"></i></a>
                    </li>
                </ul> <!-- end contact-social -->

            </div> <!-- end contact-info -->
        </div> <!-- end contact-secondary -->

    </div> <!-- end contact-content -->

</section> <!-- end s-contact -->

<?php
include_once 'footer.php'
?>