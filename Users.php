<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/25/2018
 * Time: 11:49 PM
 */
include ('Connection.php');

class Users extends Connection
{
    private $type = 'user';
    private $firstName;
    private $lastName;
    private $email;
    private $mobileNumber = "NULL";
    private $pass;
    private $dob = "NULL";
    private $gender;
    private $address = "NULL";

    public function set($data = array()){
        if (array_key_exists('type', $data)) {
            $this->type = $data['type'];
        }
        if (array_key_exists('firstName', $data)) {
            $this->firstName = $data['firstName'];
        }
        if (array_key_exists('lastName', $data)) {
            $this->lastName = $data['lastName'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('mobileNumber', $data)) {
            $this->mobileNumber = $data['mobileNumber'];
        }
        if (array_key_exists('pass', $data)) {
            $this->pass = $data['pass'];
        }
        if (array_key_exists('dob', $data)) {
            $this->dob = $data['dob'];
        }
        if (array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists('address', $data)) {
            $this->address = $data['address'];
        }
////        var_dump($this);
        return $this->mobileNumber;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `users` (`type`, `firstName`, `lastName`, `email`, `mobileNumber`, `pass`, `dob`, `gender`, `address`) VALUES (:type, :firstName, :lastName, :email, :mobileNumber, :pass, :dob, :gender, :address)");
            $result =  $stmt->execute(array(
                ':type' => $this->type,
                ':firstName' => $this->firstName,
                ':lastName' => $this->lastName,
                ':email' => $this->email,
                ':mobileNumber' => $this->mobileNumber,
                ':pass' => $this->pass,
                ':dob' => $this->dob,
                ':gender' => $this->gender,
                ':address' => $this->address
            ));

//            var_dump('this->mobileNumber');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                $_SESSION['msg'] = 'Data successfully Inserted !!';
//                header('location:index.php');
//                echo $this->mobileNumber;
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index($email){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE  `email`='$email'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function email($email){
        try{
            $stmt = $this->con->prepare("SELECT CONCAT(firstName, ' ', lastName)FROM `users` WHERE  `email`='$email'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function user_validation($email, $pass){
        try{
            $stmt = $this->con->prepare("SELECT email, pass FROM `users` WHERE `email`='$email' AND `pass`='$pass'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function email_validation($email){
        try{
            $stmt = $this->con->prepare("SELECT email FROM `users` WHERE `email`='$email'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

//    public function view($id){
//        try{
//            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            return $stmt->fetch(PDO::FETCH_ASSOC);
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }
//
//    public function delete($id){
//        try{
//            $stmt = $this->con->prepare("DELETE FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('location:index.php');
//            }
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }

    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`users` SET `firstName` = :firstName, `lastName` = :lastName, `email` = :email, `mobileNumber` = :mobileNumber, `dob` = :dob, `gender` = :gender, `address` = :address WHERE `email` = :email;");

            $result =  $stmt->execute(array(
                ':firstName' => $this->firstName,
                ':lastName' => $this->lastName,
                ':email' => $this->email,
                ':mobileNumber' => $this->mobileNumber,
                ':dob' => $this->dob,
                ':gender' => $this->gender,
                ':address' => $this->address
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
//                echo 'Yes'.$this->email;
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}